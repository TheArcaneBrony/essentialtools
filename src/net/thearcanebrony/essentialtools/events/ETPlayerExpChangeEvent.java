package net.thearcanebrony.essentialtools.events;

import net.thearcanebrony.essentialtools.Util;
import net.thearcanebrony.essentialtools.wrappers.ETEventHandler;
import org.bukkit.GameMode;
import org.bukkit.World;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.Damageable;

import java.util.ArrayList;
import java.util.List;

public class ETPlayerExpChangeEvent extends ETEventHandler {
    @EventHandler
    public static void HandleEvent(PlayerExpChangeEvent event) {
        Player p = event.getPlayer();
        if (Util.getConfig().getBoolean("Fast XP Pickup")){
            if (p.getGameMode() != GameMode.SPECTATOR) {
                World world = p.getWorld();
                for (Entity ent : p.getNearbyEntities(0.5, 0.5, 0.5)) {
                    if (ent instanceof ExperienceOrb) {
                        ExperienceOrb orb = (ExperienceOrb) ent;
                        int xp = orb.getExperience();
                        orb.setExperience(0);
                        orb.remove();
                        String log = "Player picked up xp orb with value " + xp + ".";
                        for (int i = 0; i < xp; i++) {
                            Damageable dis = getLowestDurabilityMendingItem(getMendingItems(p));
                            if (dis != null) {
                                ItemStack is = (ItemStack) dis;
                                log += "\nFound item to repair: " + is.getItemMeta().getLocalizedName() + "(" + dis.getDamage() + ")";
                                if (dis.hasDamage()) {
                                    Util.logDebug("");
                                    int d = dis.getDamage();
                                    dis.setDamage(d - 1);
                                    log += "\nRepaired item:" + dis.getDamage();
                                }
                            } else {
                                p.giveExp(xp-i);
                                log += "\nGiving player "+(xp - i)+" xp!";
                                i=xp;
                            }
                        }
                        Util.logDebug(log);
                    }
                }
            }

        }
    }
    public static List<Damageable> getMendingItems(Player p) {
        PlayerInventory pi = p.getInventory();
        List<Damageable> items = new ArrayList<>();
        if (pi.getItemInMainHand() instanceof Damageable && pi.getItemInMainHand().getEnchantments().containsKey(Enchantment.MENDING))
            items.add((Damageable) pi.getItemInMainHand());
        if (pi.getItemInOffHand() instanceof Damageable && pi.getItemInOffHand().getEnchantments().containsKey(Enchantment.MENDING))
            items.add((Damageable) pi.getItemInOffHand());
        for (ItemStack i : pi.getArmorContents())
            if (i instanceof Damageable && i.getEnchantments().containsKey(Enchantment.MENDING))
                items.add((Damageable) i);
        return items;
    }

    public static Damageable getLowestDurabilityMendingItem(List<Damageable> items) {
        int du = 0;
        Damageable li = null;
        for (Damageable i : items) {
            int dl = i.getDamage();
            if (i.hasDamage() && dl > du) {
                du = dl;
                li = i;
            }
        }
        return li;
    }
}
