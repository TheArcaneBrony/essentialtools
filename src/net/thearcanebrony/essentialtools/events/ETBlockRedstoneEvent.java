package net.thearcanebrony.essentialtools.events;

import net.thearcanebrony.essentialtools.RuntimeStore;
import net.thearcanebrony.essentialtools.mechanics.RedstoneEventTracking;
import net.thearcanebrony.essentialtools.wrappers.ETEventHandler;
import org.bukkit.Chunk;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockRedstoneEvent;

public class ETBlockRedstoneEvent extends ETEventHandler {
    @EventHandler
    public void RedstoneEvent(BlockRedstoneEvent e) {
        Chunk a = e.getBlock().getLocation().getChunk();
        RuntimeStore.RedstoneEvents.get(RedstoneEventTracking.CurrentTick).add(new Integer[]{a.getX(), a.getZ()});
    }
}
