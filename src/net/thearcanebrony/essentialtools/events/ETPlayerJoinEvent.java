package net.thearcanebrony.essentialtools.events;

import net.thearcanebrony.essentialtools.datastorage.PerPlayerData;
import net.thearcanebrony.essentialtools.wrappers.ETEventHandler;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

import java.util.List;

public class ETPlayerJoinEvent extends ETEventHandler {
    static ScoreboardManager
            scoreboardManager = Bukkit.getScoreboardManager();
    @EventHandler
    public static void HandleEvent(org.bukkit.event.player.PlayerJoinEvent e) {
        HandlePlayerJoin(e.getPlayer());
    }

    public static void HandlePlayerJoin(Player p) {
        PerPlayerData ppd = PerPlayerData.getPlayerData(p);
        Scoreboard board = scoreboardManager.getNewScoreboard();
        p.setScoreboard(board);
        ppd.scoreboard = board;
        ppd.objective = board.registerNewObjective("sidebar", "dummy");
        p.sendMessage(getConfig().getString("Welcome Message"));
        for (String cmd : getConfig().getStringList("Commands On Join")) p.performCommand(cmd);
        for (String cmd : getConfig().getStringList("Player Data." + p.getName() + ".Commands On Join"))
            p.performCommand(cmd);
        for (String cmd : getConfig().getStringList("Console Commands On Join")) p.performCommand(cmd);
        for (String cmd : getConfig().getStringList("Player Data." + p.getName() + ".Console Commands On Join"))
            p.performCommand(cmd);
        List<String> knownIps = getConfig().getStringList("Player Data." + p.getName() + ".Known IPs");
        knownIps.add(p.getAddress().getHostString());
        getConfig().set("Player Data." + p.getName() + ".Known IPs", knownIps.stream().distinct());
    }
}
