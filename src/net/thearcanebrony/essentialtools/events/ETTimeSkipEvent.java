package net.thearcanebrony.essentialtools.events;

import net.thearcanebrony.essentialtools.Util;
import net.thearcanebrony.essentialtools.wrappers.ETEventHandler;
import org.bukkit.event.EventHandler;
import org.bukkit.event.world.TimeSkipEvent;

public class ETTimeSkipEvent extends ETEventHandler {
    @EventHandler
    public static void HandleEvent(TimeSkipEvent event) {
        if (Util.getConfig().getBoolean("Fast Night.Enabled")) {
            if (event.getSkipReason() == TimeSkipEvent.SkipReason.NIGHT_SKIP) {
                event.setCancelled(true);
            }
        }
    }
}
