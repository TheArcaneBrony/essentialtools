package net.thearcanebrony.essentialtools.events;

import net.thearcanebrony.essentialtools.RuntimeStore;
import net.thearcanebrony.essentialtools.Util;
import net.thearcanebrony.essentialtools.wrappers.ETEventHandler;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.WorldBorder;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockFromToEvent;

public class ETBlockFromToEvent extends ETEventHandler {
    @EventHandler
    public static void HandleEvent(BlockFromToEvent event) {
        World world = event.getBlock().getWorld();
        WorldBorder wb = world.getWorldBorder();
        Location wbc = wb.getCenter();
        int wbs = (int) (wb.getSize() / 2);
        Block block = event.getToBlock(), oblock = event.getBlock();
        //System.out.println(block.getType().name());
        if (getConfig().getBoolean("Obsidian Generators") && oblock.getType() == Material.WATER && block.getType() == Material.REDSTONE_WIRE) {
            if (Util.CheckSurroundingBlocks(block, Material.LAVA)) {
                if (RuntimeStore.LogEvents)
                    System.out.println("Obsidian Gen 2 triggered at " + event.getBlock().getLocation());
                block.setType(Material.OBSIDIAN);
                event.setCancelled(true);
            }
        }
        if (!getConfig().getBoolean("Water Physics")) {
            if (RuntimeStore.LogEvents) System.out.println("Water flow cancelled");
            event.setCancelled(true);
        }
        if (block.getX() < wbc.getBlockX() - wbs || block.getX() > wbc.getBlockX() + wbs
                || block.getZ() < wbc.getBlockZ() - wbs || block.getZ() > wbc.getBlockZ() + wbs) {
            if (RuntimeStore.LogEvents) System.out.println("Block event outside world border cancelled");
            event.setCancelled(true);
        }
    }
}
