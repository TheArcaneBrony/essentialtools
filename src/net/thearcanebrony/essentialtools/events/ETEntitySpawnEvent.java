package net.thearcanebrony.essentialtools.events;

import net.thearcanebrony.essentialtools.mechanics.BloodMoon;
import net.thearcanebrony.essentialtools.wrappers.ETEventHandler;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Random;

public class ETEntitySpawnEvent extends ETEventHandler {
    static Random rnd = new Random();
    @EventHandler
    public static void HandleEvent(EntitySpawnEvent event) {
        World w = event.getEntity().getWorld();
        Entity e = event.getEntity();
        if(e instanceof LivingEntity){
            if(BloodMoon.bloodMoon) {
                if (e instanceof Monster) {
                    Monster m = (Monster) e;
                    if (m.getTicksLived() <= 10) {
                        m.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, BloodMoon.GetTimeUntilDay(w), 0, true, false));
                        m.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, BloodMoon.GetTimeUntilDay(w), 2, true, false));
                        m.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, BloodMoon.GetTimeUntilDay(w), 1, true, false));
//                                        if (e instanceof Creeper) {
//                                            Creeper c = (Creeper) e;
//                                            CraftCreeper cc = (CraftCreeper) c;
//                                            if (c.getFuseTicks() > 0) {
//                                                for (PotionEffect pe : c.getActivePotionEffects()) {
//                                                    c.removePotionEffect(pe.getType());
//                                                }
//                                            }
//                                        }
                        if (m.getMaxHealth() > m.getHealth()) {
                            m.setHealth(m.getMaxHealth());
                        }
                    }
                }
            }
        }
    }
}
