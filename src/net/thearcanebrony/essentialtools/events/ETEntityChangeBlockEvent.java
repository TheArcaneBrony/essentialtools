package net.thearcanebrony.essentialtools.events;

import net.thearcanebrony.essentialtools.RuntimeStore;
import net.thearcanebrony.essentialtools.wrappers.ETEventHandler;
import org.bukkit.World;
import org.bukkit.entity.Enderman;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityChangeBlockEvent;

public class ETEntityChangeBlockEvent extends ETEventHandler {
    @EventHandler
    public static void HandleEvent(EntityChangeBlockEvent event) {
        World world = event.getBlock().getWorld();
        if (event.getEntity() instanceof Enderman && getConfig().getBoolean("Block Enderman Griefing")) {
            if (RuntimeStore.LogEvents) System.out.println("Enderman tried to change block!");
            event.setCancelled(true);
        }
    }
}
