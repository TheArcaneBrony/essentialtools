package net.thearcanebrony.essentialtools.events;

import net.thearcanebrony.essentialtools.Util;
import net.thearcanebrony.essentialtools.wrappers.ETEventHandler;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.util.Random;

public class ETEntityDeathEvent extends ETEventHandler {
    static Random rnd = new Random();
    @EventHandler
    public static void HandleEvent(EntityDeathEvent event) {
        LivingEntity e = event.getEntity();
        if(e instanceof Player){
            Player p = (Player) e;
            Location ploc = p.getLocation();
            World w = ploc.getWorld();
            if(Util.getConfig().getBoolean("Old Player Death Item Spread")){
                for(ItemStack is : event.getDrops()){
                    Item i = w.spawn(ploc, Item.class);
                    i.setItemStack(is);
                    i.setVelocity(new Vector(rnd.nextFloat() * 1.25 - 0.75, rnd.nextFloat() * 0.75, rnd.nextFloat() * 1.25 - 0.75));
                }
                event.getDrops().clear();
            }
        }
    }
}
