package net.thearcanebrony.essentialtools.events;

import net.thearcanebrony.essentialtools.RuntimeStore;
import net.thearcanebrony.essentialtools.wrappers.ETEventHandler;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockPhysicsEvent;

public class ETBlockPhysicsEvent extends ETEventHandler {
    @EventHandler
    public static void HandleEvent(BlockPhysicsEvent event) {
        Material mat = event.getBlock().getType();
        if (getConfig().getBoolean("Classic Liquids") && (mat == Material.WATER || mat == Material.LAVA)) {
            if (RuntimeStore.LogEvents)
                System.out.println("Classic liquids triggered at " + event.getBlock().getLocation());
            event.getBlock().setType(event.getBlock().getType());
            //event.getBlock().setBlockData(Bukkit.createBlockData("{level: 15}"));
        }
        if (getConfig().getBoolean("Obsidian Generator") && event.getChangedType() == Material.REDSTONE_WIRE) {
            if (RuntimeStore.LogEvents)
                System.out.println("Obsidian Gen triggered at " + event.getBlock().getLocation());
            event.getBlock().setType(Material.OBSIDIAN);
        }
        if (!getConfig().getBoolean("Grass Spreading") && (mat == Material.GRASS_BLOCK || mat == Material.DIRT)) {
            event.setCancelled(true);
            return;
        }
			/*if(event.getChangedType().name().equals(event.getSourceBlock().getType().name()))event.setCancelled(true);
			else System.out.println(event.getChangedType().name() + " -> "+event.getSourceBlock().getType().name());*/
    }
}
