package net.thearcanebrony.essentialtools.events;

import net.thearcanebrony.essentialtools.FurnaceUtil;
import net.thearcanebrony.essentialtools.Main;
import net.thearcanebrony.essentialtools.RuntimeStore;
import net.thearcanebrony.essentialtools.wrappers.ETEventHandler;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class ETPlayerInteractEvent extends ETEventHandler {
    @EventHandler
    public static void HandleEvent(PlayerInteractEvent event) {

        if (RuntimeStore.LogEvents) System.out.println("PlayerInteractEvent!");
        Player p = event.getPlayer();
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            Block bl = event.getClickedBlock();
            if (getConfig().getBoolean("Shift Click Furnace XP") && bl.getType() == Material.FURNACE && p.isSneaking()) {
                if (RuntimeStore.LogEvents) System.out.println("Furnace Shift Clicked!");
                FurnaceUtil.DropFurnaceXP(bl, p.rayTraceBlocks(8).getHitPosition().toLocation(p.getWorld()));
                event.setCancelled(true);
            }
            if (getConfig().getBoolean("Stair Sitting") && bl.getType().toString().contains("STAIR")) {
                Location loc = bl.getLocation();
                loc.add(0.5, -1.00, 0.5);
                Horse ent = (Horse) p.getWorld().spawnEntity(loc, EntityType.HORSE);
                ent.addPassenger(p);
                ent.setAI(false);
				/*((CraftHorse) ent).getHandle().setInvisible(true);
				((CraftHorse) ent).getHandle().setTamed(true);
				((CraftHorse) ent).getHandle().setSilent(true);
				((CraftHorse) ent).getHandle().setInvulnerable(true);*/
                // ((LivingEntity)
                // ((CraftHorse)ent).getHandle()).setCollidable(false);
                ent.setGravity(false);
                ent.setAdult();
                //ent.addPotionEffects((Collection<PotionEffect>) new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 1));
                p.getServer().dispatchCommand(p.getServer().getConsoleSender(), "minecraft:effect @e[type=horse] minecraft:invisibility 1000000 1 true");
                //p.getServer().dispatchCommand(new CustomConsoleCommandSender(), "minecraft:effect @e[type=horse] minecraft:invisibility 1000000 1 true");
                //
                event.setCancelled(true);
            }
        }
        if (getConfig().getBoolean("Beta Bows")) {
            if (event.getItem() != null && event.getItem().getType() == Material.BOW) {
                if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                    event.getPlayer().launchProjectile(Arrow.class);
                    event.getPlayer().getWorld().playSound(event.getPlayer().getLocation(), Sound.ENTITY_SKELETON_SHOOT, 1f, 1f);
                    event.setCancelled(true);
                }
            }
        }
        Main.wehandler.onPlayerUse(event);
    }
}
