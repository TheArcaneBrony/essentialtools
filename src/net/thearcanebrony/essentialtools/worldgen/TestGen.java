package net.thearcanebrony.essentialtools.worldgen;

import org.bukkit.World;
import org.bukkit.generator.ChunkGenerator;

import java.util.Random;

public class TestGen extends ChunkGenerator {
    Random rnd = new Random();

    byte[][] result;
    double height = 8;

    public byte[][] generateBlockSections(World world, Random random, int chunkX, int chunkZ, BiomeGrid biomeGrid) {

        result = new byte[world.getMaxHeight() / 16][];
        // world height / chunk part height (=16, look above)
        try {
            if (Math.abs(chunkX) >= 16 || Math.abs(chunkZ) >= 16) {

                for (int x = 0; x < 16; x++) {
                    for (int z = 0; z < 16; z++) {
                        try {
                            setBlock(result, x, 0, z, (byte) 7); //bedrock
                            // for(int y= 1; y<2,a2)); y++)
                            setBlock(result, x, (int) Math.min(255,
                                    Math.max(0,
                                            Math.abs((Math.cos(chunkX * 16 + x) * Math.sin(chunkZ * 16 + z)) * 16))),
                                    z, (byte) 3); // cobblestone
                        } catch (Exception e) {
                        }
                    }
                }
            } else {
                for (int x = 0; x < 16; x++) {

                    for (int z = 0; z < 16; z++) {
                        try {
                            for (int y = 1; y < 16; y++)
                                setBlock(result, x, y, z, (byte) 9); // water
                        } catch (Exception e) {
                        }
                    }
                }
            }

            // bedrock
            for (int x = 0; x < 16; x++) {

                for (int z = 0; z < 16; z++) {
                    try {
                        setBlock(result, x, 0, z, (byte) 7); //bedrock
                    } catch (Exception e) {
                    }
                }
            }

        } catch (Exception e) {
            System.out.println("Exception ocurred generating chunk " + chunkX + " " + chunkZ + ":");
            e.printStackTrace();
        }
        return result;
    }

    void setBlock(byte[][] result, int x, int y, int z, byte blkid) {
        try {
            // is this chunk part already initialized?
            if (result[y >> 4] == null) {
                // Initialize the chunk part
                result[y >> 4] = new byte[4096];
            }
            // set the block (look above, how this is done)
            result[y >> 4][((y & 0xF) << 8) | (z << 4) | x] = blkid;
        } catch (Exception e) {
            System.out.println("Exception ocurred setting block " + x + " " + y + " " + z + ":");
            // e.printStackTrace();
        }
    }
}