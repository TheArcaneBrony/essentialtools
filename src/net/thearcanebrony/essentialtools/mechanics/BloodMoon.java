package net.thearcanebrony.essentialtools.mechanics;

import net.thearcanebrony.essentialtools.Main;
import net.thearcanebrony.essentialtools.Util;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BloodMoon {
    public static boolean nightProcessed = false, bloodMoon = false;
    public static List<Integer> knownEntities = new ArrayList<Integer>();
    public static Random rnd = new Random();
    public static double ParticleDensity = 0;
    public static double Chance = 100;
    public static World overworld;

    public static void Start() {
        overworld = Util.getServer().getWorld("world");
        if (Util.getConfig().getBoolean("Blood Moon.Enabled")) {
            ParticleDensity = Util.getConfig().getDouble("Blood Moon.Particle Distance Multiplier");
            Chance = Util.getConfig().getDouble("Blood Moon.Chance");
            System.out.println("BLOOD MOON IS ENABLED!");
            System.out.println("Particle density: " + ParticleDensity);
            new BukkitRunnable() {
                @Override
                public void run() {

                    if(!Main.IsRunning) this.cancel();
                    Long time = overworld.getTime();
                    if (time > 14000) {
                        if (!nightProcessed) {
                            nightProcessed = true;
                            bloodMoon = rnd.nextInt(101) <= Chance;
                            if (bloodMoon) {
                                for (Player p : overworld.getPlayers()) {
                                    p.sendMessage(Util.getConfig().getString("Chat Prefix") + "A blood moon has started!");
                                }
                            }
                        }
                        if (bloodMoon) {
                            for (Player p : overworld.getPlayers()) {
                                Location loc = p.getLocation();
                                double sx = loc.getX() - 20, ex = loc.getX() + 20,
                                        sz = loc.getZ() - 20, ez = loc.getZ() + 20;
                                for (double x = sx; x < ex; x += rnd.nextFloat() * ParticleDensity) {
                                    for (double z = sz; z < ez; z += rnd.nextFloat() * ParticleDensity) {
                                        Location loc2 = new Location(overworld, x, loc.getY() - 10 + rnd.nextInt(20), z);
                                        Material m = overworld.getBlockAt(loc2).getType();
                                        if (m.isTransparent())
                                            p.spawnParticle(Particle.FLAME, loc2, 1);
                                        //System.out.println(Particle.BLOCK_DUST.getDataType());
                                    }
                                }
                            }
                        }
                    } else {
                        nightProcessed = bloodMoon = false;
                        knownEntities.clear();
                    }
                    //System.out.println(overworld.getTime());
                }
            }.runTaskTimer(Util.getPlugin(), 0L, 5L);
        }
    }
    public static int GetTimeUntilDay(World w){
        return 24000 - (int)w.getTime();
    }
}
