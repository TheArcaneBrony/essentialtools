package net.thearcanebrony.essentialtools.mechanics;

import net.thearcanebrony.essentialtools.Main;
import net.thearcanebrony.essentialtools.RuntimeStore;
import net.thearcanebrony.essentialtools.Util;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class BarrierView {
    public static void Start() {
        System.out.println(Util.getPlugin());
        new BukkitRunnable() {
            @Override
            public void run() {

                if(!Main.IsRunning) this.cancel();
                for (Player p : RuntimeStore.barrierViewers) {
                    Location ploc = p.getLocation();
                    int r = 16;
                    for (int x = ploc.getBlockX() - r; x <= ploc.getBlockX() + r; x++) {
                        for (int y = Math.max(0, ploc.getBlockY() - r); y <= Math.max(0,
                                Math.min(256, ploc.getBlockY() + r)); y++) {
                            for (int z = ploc.getBlockZ() - r; z <= ploc.getBlockZ() + r; z++) {
                                Location bloc = new Location(p.getWorld(), x + 0.5, y + 0.5, z + 0.5);
                                if (p.getWorld().getBlockAt(bloc).getType() == Material.BARRIER) {
                                    p.spawnParticle(Particle.BARRIER, bloc, 0);
                                }
                            }
                        }
                    }
                }
            }
        }.runTaskTimer(Util.getPlugin(), 0L, 20L);
    }
}
