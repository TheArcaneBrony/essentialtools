package net.thearcanebrony.essentialtools.mechanics;

import net.thearcanebrony.essentialtools.Main;
import net.thearcanebrony.essentialtools.Util;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class EntityHealthBar {
    public static void Start() {
            BukkitRunnable br = new BukkitRunnable() {
                @Override
                public void run() {

                    if(!Main.IsRunning) this.cancel();
                    for (World w : Util.getServer().getWorlds())
                        for (Entity e : w.getEntities()) {
                            if (e instanceof LivingEntity && !(e instanceof Player)) {
                                LivingEntity m = (LivingEntity) e;

                                if (Util.getConfig().getBoolean("Entity Health Bars")){
                                    double h = m.getHealth(), mh = m.getMaxHealth();
                                    e.setCustomName(e.getType().name() + " " + ChatColor.RED + "|".repeat((int) m.getHealth()) + ChatColor.WHITE + "|".repeat((int) (m.getMaxHealth() - m.getHealth())));
                                    e.setCustomNameVisible(h < mh);
                                }
                                else if(e.getCustomName() != null){
                                    if(e.getCustomName().startsWith(e.getType().name() + " " + ChatColor.RED))
                                    e.setCustomName("");
                                    e.setCustomNameVisible(false);
                                }
                            }
                        }
                }
            };
            if(Util.getConfig().getBoolean("Entity Health Bars"))
                br.runTaskTimer(Util.getPlugin(), 0L, 5L);
            else
                br.runTaskTimer(Util.getPlugin(), 0L, 20L);
    }
}
