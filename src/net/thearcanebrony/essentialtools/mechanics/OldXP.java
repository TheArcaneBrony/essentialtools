package net.thearcanebrony.essentialtools.mechanics;

import net.thearcanebrony.essentialtools.Main;
import net.thearcanebrony.essentialtools.Util;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_17_R1.CraftServer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.io.IOException;

public class OldXP {
    public static void Start() {
        if (Util.getConfig().getBoolean("Split XP Orbs")) {
//            Util.getServer().spigot().getSpigotConfig().getList("worlds");


            YamlConfiguration cfg = Bukkit.spigot().getConfig();
            if (cfg.getDouble("world-settings.default.merge-radius.exp") != 0.0d) {
                cfg.set("world-settings.default.merge-radius.exp", 0.0d);
                System.out.println("Saving " + new File(".").getAbsolutePath() + "/spigot.yml, please restart your server!");
                try {
                    cfg.save(new File(".").getAbsolutePath() + "/spigot.yml");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            new BukkitRunnable() {
                @Override
                public void run() {

                    if(!Main.IsRunning) this.cancel();
                    for (Player p : Bukkit.getOnlinePlayers()) {
                        World world = p.getWorld();

                        for (Entity ent : p.getNearbyEntities(30, 30, 30)) {
                            if (ent instanceof ExperienceOrb) {
                                ExperienceOrb orb = (ExperienceOrb) ent;
                                Location loc = orb.getLocation();
                                if(orb.getExperience() > 1 && orb.getNearbyEntities(16,16,16).size() > 500){
                                    int n = orb.getExperience() / 2;
                                    Util.SpawnXpOrb(orb, n);
                                    orb.setExperience(n);
                                }
//                                if (orb.getExperience() != 500 && orb.getExperience() != 1) {
//                                    if (orb.getExperience() >= 500) {
//                                        for (int i = 0; i < orb.getExperience() / 500; i++) {
//                                            Util.SpawnXpOrb(orb, 500);
//                                            orb.setExperience(orb.getExperience() - 500);
//                                        }
//                                        Util.SpawnXpOrb(loc, orb.getExperience());
//                                        orb.remove();
//                                    } else if (orb.getExperience() > 1) {
//                                        for (int i = 0; i < orb.getExperience(); i++) {
//                                            Util.SpawnXpOrb(loc, 1);
//                                        }
//                                        orb.remove();
//                                    }
//                                }
                            }
                        }
                    }
                }
            }.runTaskTimer(Util.getPlugin(), 0L, 2L);
        }
    }
}
