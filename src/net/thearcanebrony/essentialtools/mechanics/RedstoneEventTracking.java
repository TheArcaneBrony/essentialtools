package net.thearcanebrony.essentialtools.mechanics;

import net.thearcanebrony.essentialtools.Main;
import net.thearcanebrony.essentialtools.RuntimeStore;
import net.thearcanebrony.essentialtools.Util;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class RedstoneEventTracking {
    public static long CurrentTick = 0;

    public static void Start() {
        new BukkitRunnable() {
            @Override
            public void run() {
                if(!Main.IsRunning) this.cancel();
                List<Long> keys = new ArrayList<>();
                for (Long item : RuntimeStore.RedstoneEvents.keySet()) {
                    if (item < CurrentTick - 20) {
                        keys.add(item);
                    }
                }
                for (Long item : keys) {
                    RuntimeStore.RedstoneEvents.remove(item);
                }
                keys.clear();
//				for (Map.Entry<Long,ArrayList<Integer[]>> i : RedstoneEvents.entrySet()) {
//					for (Integer[] j : i.getValue()) {
//						System.out.println(i.getKey()+": " + j[0] + "/"+ j[1]);
//					}
//				}
//				System.out.println("--------------");
                RuntimeStore.RedstoneEvents.put(CurrentTick + 1, new ArrayList<Integer[]>());
                CurrentTick++;
                //Bukkit.getServer().broadcastMessage("Redstone events: " + RedstoneEvents.size());
            }
        }.runTaskTimer(Util.getPlugin(), 0L, 1L);
    }
}
