package net.thearcanebrony.essentialtools.mechanics;

import net.thearcanebrony.essentialtools.Main;
import net.thearcanebrony.essentialtools.Util;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

public class FastNight {
    static int ticksToRun = 10, maxSpeed = 10;

    public static void Start() {
        if (Util.getConfig().getBoolean("Fast Night.Enabled")) {
            maxSpeed = Util.getConfig().getInt("Fast Night.Max Speed");
            new BukkitRunnable() {
                @Override
                public void run() {

                    if(!Main.IsRunning) this.cancel();
                    boolean anyoneSleeping = false;
                    for (Player p : Bukkit.getOnlinePlayers()) {
                        if (p.isSleeping()) {
                            anyoneSleeping = true;
                        }
                    }
                    if (anyoneSleeping)
                        try {
                            //TODO: implement this in Lag.java
                            if (Util.getMspt() >= 50)
                                ticksToRun--;
                            else
                                ticksToRun += 2;
                            //System.out.println(ticksToRun);
                            if (ticksToRun > maxSpeed) ticksToRun = maxSpeed;
                            for (int i = 0; i < ticksToRun; i++) {
                                Util.forceTickNow();
                            }
                        } catch (ReflectiveOperationException e) {
                            e.printStackTrace();
                        }
                }
            }.runTaskTimer(Util.getPlugin(), 0L, 1L);
            Plugin pl = Bukkit.getPluginManager().getPlugin("SinglePlayerSleep");
            if (pl != null) {
                Bukkit.getPluginManager().disablePlugin(pl);
            }
        }
    }
}
