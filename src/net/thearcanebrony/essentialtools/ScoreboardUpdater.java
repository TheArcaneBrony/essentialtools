package net.thearcanebrony.essentialtools;

import net.thearcanebrony.essentialtools.datastorage.PerPlayerData;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

import java.util.List;

public class ScoreboardUpdater {

    private static ScoreboardManager scoreboardManager = Bukkit.getScoreboardManager();
    private static long lastTimeRun = 0, lastTick = 0, timeSinceLastTick = 0;

    private static Scoreboard board;
    private static Objective objective;
    private static int numScores;

    public static void StartTask() {
        scoreboardManager = Bukkit.getScoreboardManager();
        Runtime runtime = Runtime.getRuntime();
        if(Util.getConfig().getBoolean("Scoreboard.Enabled"))
        new BukkitRunnable() {
            @Override
            public void run() {
                if(!Main.IsRunning) this.cancel();
                int len = 0;
                lastTick = System.nanoTime() / 1000000;
                if (lastTick >= lastTimeRun + 1000) {
                    timeSinceLastTick = lastTick - lastTimeRun;
                    lastTimeRun = System.nanoTime() / 1000000;
                    RuntimeStore.MemUsed = runtime.totalMemory() - runtime.freeMemory();
                    RuntimeStore.MemMax = runtime.maxMemory() / 1024;
                    RuntimeStore.mempercent = (RuntimeStore.MemUsed.doubleValue() / RuntimeStore.MemMax.doubleValue()) / 1024 * 100;
                    for (Player p : Util.getServer().getOnlinePlayers()) {
                        if (Util.getConfig().getBoolean("Tablist.Enabled")) {
                            //p.setPlayerListHeaderFooter(new TextComponent(), new TextComponent());
                            len = 0;
                            for (String line : Util.getConfig().getStringList("Tablist.Header")) {
                                String sline = Util.parseVars(line, p);
                                if (sline.length() > len) len = sline.length();
                            }
                            for (String line : Util.getConfig().getStringList("Tablist.Footer")) {
                                String sline = Util.parseVars(line, p);
                                if (sline.length() > len) len = sline.length();
                            }
                            try {
                                p.setPlayerListHeaderFooter(Util.parseVars(String.join("\n", Util.getConfig().getStringList("Tablist.Header")), p, len), Util.parseVars(String.join("\n", Util.getConfig().getStringList("Tablist.Footer")), p, len));

                            } catch (Exception e) {
                                System.out.println("Failed to set player list info for player " + p.getName() + ":");
                                e.printStackTrace();
                            }
                        }

                        if (Util.getConfig().getBoolean("Tablist.Enabled")) {
                            try {
                                PerPlayerData ppd = PerPlayerData.getPlayerData(p);
                                len = 0;
                                objective = ppd.objective;
                                if (!Util.getConfig().contains("Player Data." + p.getName() + ".Scoreboard Enabled") || Util.getConfig().getBoolean("Player Data." + p.getName() + ".Scoreboard Enabled")) {

                                    objective.setDisplaySlot(DisplaySlot.SIDEBAR);
                                    objective.setDisplayName(Util.parseVars(Util.getConfig().getString("Sidebar.Header"), p));
                                    //						numScores = getConfig().getInt("Sidebar.Count");

                                    List<String> lines = Util.getConfig().getStringList("Sidebar.Lines");
                                    numScores = lines.size();

                                    //System.out.println(len);
                                    for (String line : lines) {
                                        String sline = Util.parseVars(line, p, len);

                                        if (sline.length() > 40) sline = sline.substring(0, 40);
                                        //System.out.println(sline.replace("§", "&"));
                                        try {
                                            objective.getScore(sline).setScore(--numScores);
                                        } catch (Exception e) {
                                            System.out.println("STRING TOO LONG FOR SCOREBOARD: " + sline);
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                System.out.println("Failed to set scoreboard for player " + p.getName() + ":");
                                e.printStackTrace();
                            }
                        }
                        RuntimeStore.colorIteration++;
                    }
                }
            }
        }.runTaskTimerAsynchronously(Util.getPlugin(), 0L, 5L);
    }
}
