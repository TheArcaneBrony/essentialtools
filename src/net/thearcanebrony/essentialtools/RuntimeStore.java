package net.thearcanebrony.essentialtools;

import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RuntimeStore {

    public static boolean LogEvents = Util.getConfig().getBoolean("Debug.Log Events");
    public static boolean LogDebug = Util.getConfig().getBoolean("Debug.Log Debug Info");
    public static boolean LogUnknownMobs = Util.getConfig().getBoolean("Debug.Log Unknown Entities");
    public static ArrayList<BossBar> Bossbars = new ArrayList<BossBar>();
    public static ArrayList<BossBar> PingBars = new ArrayList<BossBar>();
    public static List<Player> barrierViewers = new ArrayList<Player>();
    public static final HashMap<Long, ArrayList<Integer[]>> RedstoneEvents = new HashMap<>();
    public static int colorIteration = 0;
    public static Long MemUsed, MemMax;
    public static Double mempercent;
    public static int entities = 0, numScores;
}
