package net.thearcanebrony.essentialtools.worldedit;

import net.thearcanebrony.essentialtools.CommandHandler;
import net.thearcanebrony.essentialtools.Lag;
import net.thearcanebrony.essentialtools.Main;
import net.thearcanebrony.essentialtools.RuntimeStore;
import net.thearcanebrony.essentialtools.datastorage.WETaskManager;
import net.thearcanebrony.essentialtools.datastorage.WETaskManager.Task;
import org.bukkit.*;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

import java.text.DecimalFormat;
import java.util.Random;

public class SetbelowT extends CommandHandler {
    Random rnd = new Random();

    public SetbelowT(Main plugin) {
        super(plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        Player p = (Player) sender;
        if (!sender.hasPermission("seaponyplugin.worldedit")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission.");
            return true;
        }

        int i = 0;
        long startTime = System.nanoTime();


        if (args.length > 0 && Material.getMaterial(args[0]) == null)
            return true;
        if (args.length == 1) {
            for (int y = 0; y < p.getLocation().getBlockY(); y++) {
                p.getWorld().getBlockAt(p.getLocation().getBlockX(), y, p.getLocation().getBlockZ())
                        .setType(Material.getMaterial(args[0]));
                i++;
            }
        } else if (args.length == 2) {
            final int r = Integer.parseInt(args[1]);
            final Location sloc = p.getLocation();
            final int px = sloc.getBlockX(), pz = sloc.getBlockZ();
            p.sendMessage("Finding highest Y...");
            final int py = GetHighestY(p, px, sloc.getBlockY(), pz, r, args[0]);
            p.sendMessage("Found heighest Y: " + py);
            if (py < 0) return true;
            Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
                @Override
                public void run() {
                    int _i = 0;
                    int _r = 0;
                    int DelayedTicks = 0;


                    BossBar task = Bukkit.createBossBar("TASKS...", BarColor.GREEN, BarStyle.SEGMENTED_20);
                    RuntimeStore.Bossbars.add(task);
                    task.addPlayer(p);
                    Task tasks = new Task();
                    WETaskManager.AddTask(p, tasks);
                    for (int rad = _r; rad < r; rad++) {

                        long _startTime = System.nanoTime();
                        int _i2 = 0;

                        for (int x = px - rad; x <= px + rad; x++) {
                            for (int z = pz - rad; z <= pz + rad; z++) {
                                for (int y = 0; y < py; y++) {
                                    int yskipped = 0;
                                    if (tasks.Canceled) {
                                        task.removeAll();
                                        return;
                                    }
                                    final int _x = x, _y = y, _z = z, __i = _i;
                                    if (p.getWorld().getBlockAt(_x, _y, _z).getType().name().equals(args[0]))
                                        yskipped++;
                                    else if (!tasks.Canceled && !tasks.tasks.containsKey(x + "/" + y + "/" + z)) {
                                        _i++;
                                        _i2++;
                                        //final BukkitTask _t;
                                        //p.sendMessage(p.getWorld().getBlockAt(_x, _y, _z).getType().name());
                                        final BukkitTask t = Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
                                            int curr = __i;
                                            //final BukkitTask _t = t;

                                            @Override
                                            public void run() {
                                                if (curr == tasks.tasks.size() || tasks.Canceled) {
                                                    task.removePlayer(p);
                                                    return;
                                                }
                                                p.spawnParticle(Particle.WATER_BUBBLE, new Location(p.getWorld(), _x, _y, _z), 1, 0, 0, 0, 0);
                                                p.getWorld().getBlockAt(_x, _y, _z)
                                                        .setType(Material.getMaterial(args[0]));
                                                // i++;
                                                task.setTitle("Worldedit task: " + curr + "/" + tasks.tasks.size() + " (Expected block count: " + Math.pow(r * 2, 2) * py + ") Current block: " + _x + "/" + _y + "/" + _z);
                                                task.setProgress(curr / tasks.tasks.size());

                                                //tasks.remove(_t);
                                            }
                                        }, (long) (sloc.distance(new Location(sloc.getWorld(), _x, _y + yskipped, _z)) + (py - _y) + (20 - Lag.getTPS())));
                                        //}, (long) (((Math.pow(rad, 2))/(900*10))+(rnd.nextFloat()*5)));
                                        //}, (long) (rnd.nextFloat() * (rad * 2 + _r * 5)) + (_y / 2));
                                        //_t = t;
                                        if (!tasks.Canceled) tasks.tasks.put(x + "/" + y + "/" + z, t);
                                        DelayedTicks--;
                                        if (DelayedTicks < 0) DelayedTicks = 0;

                                        // },
                                        // (long)(rnd.nextFloat()*(_i/500))+(_y/2)
                                        // ));

                                    }
                                    if (Lag.getTPS() <= 19)
                                        try {
                                            DelayedTicks += 1;
                                            Thread.sleep(DelayedTicks);
                                        } catch (InterruptedException e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                        }
                                }
                            }
                            _r = rad;
                        }
                        p.sendMessage(ChatColor.YELLOW + "Queued " + _i2 + " blocks (" + tasks.tasks.size() + " tasks) (r=" + rad + ") in "
                                + new DecimalFormat("#.###").format(((System.nanoTime() - _startTime) / 1000000d))
                                + " ms.");
                    }

                    p.sendMessage(ChatColor.YELLOW + "Queued " + _i + " blocks (" + tasks.tasks.size() + " tasks) total in "
                            + new DecimalFormat("#.###").format(((System.nanoTime() - startTime) / 1000000d))
                            + " ms.");
                    boolean running = true;
                    while (running) {
                        String oldname = task.getTitle();
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        if (oldname.equals(task.getTitle())) task.removePlayer(p);
                    }

                }
            });

        }

        return true;
    }

    public int GetHighestY(Player p, int x, int y, int z, int radius, String BlockToFind) {
        boolean HasReachedGround = false;
        int HighestY = y;
        while (!HasReachedGround) {
            for (int _x = x - radius; _x <= x + radius; _x++) {
                for (int _z = z - radius; _z <= z + radius; _z++) {
                    if (!p.getWorld().getBlockAt(_x, HighestY, _z).getType().name().equals(BlockToFind))
                        return HighestY;
                }
            }
            HighestY--;
        }
        return HighestY;
    }

}
