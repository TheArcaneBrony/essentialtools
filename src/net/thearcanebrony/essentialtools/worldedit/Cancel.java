package net.thearcanebrony.essentialtools.worldedit;

import net.thearcanebrony.essentialtools.CommandHandler;
import net.thearcanebrony.essentialtools.Main;
import net.thearcanebrony.essentialtools.datastorage.WETaskManager;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Cancel extends CommandHandler {
    public Cancel(Main plugin) {
        super(plugin);
        // TODO Auto-generated constructor stub
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player p = (Player) sender;
        if (!sender.hasPermission("essentialtools.worldedit")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission.");
            return true;
        }
        p.sendMessage("Cancelled " + WETaskManager.CancelTasks(p) + " tasks.");

        return true;
    }

}
