package net.thearcanebrony.essentialtools.worldedit;

import net.thearcanebrony.essentialtools.CommandHandler;
import net.thearcanebrony.essentialtools.datastorage.WEPerPlayerData;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.HashMap;
import java.util.Random;
import java.util.Set;

public class WEHandler extends CommandHandler implements Listener {
    public static HashMap<Player, WEPerPlayerData> playerData = new HashMap<Player, WEPerPlayerData>();
    Random rnd = new Random();

    public WEHandler() {
        super(plugin);
    }

    public static WEPerPlayerData getPlayerData(Player p) {

        return playerData.getOrDefault(p, getDefaultPlayerData(p));
    }

    public static WEPerPlayerData getDefaultPlayerData(Player p) {
        WEPerPlayerData dpd = new WEPerPlayerData(p.getServer());
        playerData.putIfAbsent(p, dpd);
        return playerData.getOrDefault(p, dpd);
    }

    @SuppressWarnings("deprecation")
    public void onPlayerUse(PlayerInteractEvent event) {
        Player p = event.getPlayer();
        WEPerPlayerData pd = getPlayerData(p);
        if (p.getItemInHand().getType() == Material.PRISMARINE_SHARD) {
            if (event.getAction() == Action.LEFT_CLICK_BLOCK) {
                pd.pos1 = p.getTargetBlock((Set<Material>) null, 5).getLocation();
                p.sendMessage(ChatColor.RED + "Set position 1 to " + pd.pos1.getBlockX() + " " + pd.pos1.getBlockY() + " "
                        + pd.pos1.getBlockZ() + "!");
            } else if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                pd.pos2 = p.getTargetBlock((Set<Material>) null, 5).getLocation();
                p.sendMessage(ChatColor.RED + "Set position 2 to " + pd.pos2.getBlockX() + " " + pd.pos2.getBlockY() + " "
                        + pd.pos2.getBlockZ() + "!");
            }
            Effect e = Effect.MOBSPAWNER_FLAMES;
            for (int x = Math.min(pd.pos1.getBlockX(), pd.pos2.getBlockX()); x <= Math.max(pd.pos1.getBlockX(), pd.pos2.getBlockX()); x++) {
                p.getWorld().playEffect(new Location(p.getWorld(), x + 0.5d, pd.pos1.getY() + 0.5d, pd.pos1.getZ() + 0.5d), e, 31);
                p.getWorld().playEffect(new Location(p.getWorld(), x + 0.5d, pd.pos2.getY() + 0.5d, pd.pos1.getZ() + 0.5d), e, 31);
                p.getWorld().playEffect(new Location(p.getWorld(), x + 0.5d, pd.pos1.getY() + 0.5d, pd.pos2.getZ() + 0.5d), e, 31);
                p.getWorld().playEffect(new Location(p.getWorld(), x + 0.5d, pd.pos2.getY() + 0.5d, pd.pos2.getZ() + 0.5d), e, 31);
            }
            for (int z = Math.min(pd.pos1.getBlockZ(), pd.pos2.getBlockZ()); z <= Math.max(pd.pos1.getBlockZ(), pd.pos2.getBlockZ()); z++) {
                p.getWorld().playEffect(new Location(p.getWorld(), pd.pos1.getX() + 0.5d, pd.pos1.getY() + 0.5d, z + 0.5d), e, 31);
                p.getWorld().playEffect(new Location(p.getWorld(), pd.pos2.getX() + 0.5d, pd.pos1.getY() + 0.5d, z + 0.5d), e, 31);
                p.getWorld().playEffect(new Location(p.getWorld(), pd.pos1.getX() + 0.5d, pd.pos2.getY() + 0.5d, z + 0.5d), e, 31);
                p.getWorld().playEffect(new Location(p.getWorld(), pd.pos2.getX() + 0.5d, pd.pos2.getY() + 0.5d, z + 0.5d), e, 31);
            }
            for (int y = Math.min(pd.pos1.getBlockY(), pd.pos2.getBlockY()); y <= Math.max(pd.pos1.getBlockY(), pd.pos2.getBlockY()); y++) {
                p.getWorld().playEffect(new Location(p.getWorld(), pd.pos1.getX() + 0.5d, y + 0.5d, pd.pos1.getZ() + 0.5d), e, 31);
                p.getWorld().playEffect(new Location(p.getWorld(), pd.pos2.getX() + 0.5d, y + 0.5d, pd.pos1.getZ() + 0.5d), e, 31);
                p.getWorld().playEffect(new Location(p.getWorld(), pd.pos1.getX() + 0.5d, y + 0.5d, pd.pos2.getZ() + 0.5d), e, 31);
                p.getWorld().playEffect(new Location(p.getWorld(), pd.pos2.getX() + 0.5d, y + 0.5d, pd.pos2.getZ() + 0.5d), e, 31);
            }

            event.setCancelled(true);
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        // TODO Auto-generated method stub
        return false;
    }

}
