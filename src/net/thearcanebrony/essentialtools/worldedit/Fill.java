package net.thearcanebrony.essentialtools.worldedit;

import net.thearcanebrony.essentialtools.CommandHandler;
import net.thearcanebrony.essentialtools.Main;
import net.thearcanebrony.essentialtools.datastorage.WEPerPlayerData;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.text.DecimalFormat;
import java.util.Random;

public class Fill extends CommandHandler {
    Random rnd = new Random();

    public Fill(Main plugin) {
        super(plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        Player p = (Player) sender;

        if (!sender.hasPermission("ccyt.worldedit")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission.");
            return true;
        }
        int i = 0;
        long startTime = System.nanoTime();
        WEPerPlayerData pd = WEHandler.getPlayerData(p);
        if (args.length == 1) {
            for (int x = Math.min(pd.pos1.getBlockX(), pd.pos2.getBlockX()); x <= Math.max(pd.pos1.getBlockX(), pd.pos2.getBlockX()); x++) {
                for (int y = Math.min(pd.pos1.getBlockY(), pd.pos2.getBlockY()); y <= Math.max(pd.pos1.getBlockY(), pd.pos2.getBlockY()); y++) {
                    for (int z = Math.min(pd.pos1.getBlockZ(), pd.pos2.getBlockZ()); z <= Math.max(pd.pos1.getBlockZ(), pd.pos2.getBlockZ()); z++) {
                        p.getWorld().getBlockAt(x, y, z).setType(Material.matchMaterial(args[0]));
                        i++;
                    }
                }
            }
        } else {
            for (int x = Math.min(pd.pos1.getBlockX(), pd.pos2.getBlockX()); x < Math.max(pd.pos1.getBlockX(), pd.pos2.getBlockX())
                    + 1; x++) {
                for (int y = Math.min(pd.pos1.getBlockY(), pd.pos2.getBlockY()); y < Math.max(pd.pos1.getBlockY(), pd.pos2.getBlockY())
                        + 1; y++) {
                    for (int z = Math.min(pd.pos1.getBlockZ(), pd.pos2.getBlockZ()); z <= Math.max(pd.pos1.getBlockZ(),
                            pd.pos2.getBlockZ() + 1); z++) {

                        p.getWorld().getBlockAt(x, y, z).setType(Material.matchMaterial(args[rnd.nextInt(args.length)]));
                        i++;
                    }
                }
            }
        }
        p.sendMessage(ChatColor.YELLOW + "Updated " + i + " blocks in " + new DecimalFormat("#.######").format(((System.nanoTime() - startTime) / 1000000d)) + " ms.");

        return true;
    }

}
