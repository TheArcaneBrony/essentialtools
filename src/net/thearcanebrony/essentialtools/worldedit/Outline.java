package net.thearcanebrony.essentialtools.worldedit;

import net.thearcanebrony.essentialtools.CommandHandler;
import net.thearcanebrony.essentialtools.Main;
import net.thearcanebrony.essentialtools.datastorage.WEPerPlayerData;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import java.text.DecimalFormat;
import java.util.Random;

public class Outline extends CommandHandler implements Listener {
    Random rnd = new Random();

    public Outline(Main plugin) {
        super(plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        Player p = (Player) sender;
        if (!sender.hasPermission("ccyt.worldedit")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission.");
            return true;
        }
        int i = 0;
        WEPerPlayerData pd = WEHandler.getPlayerData(p);
        long startTime = System.nanoTime();

        for (int x = Math.min(pd.pos1.getBlockX(), pd.pos2.getBlockX()); x <= Math.max(pd.pos1.getBlockX(), pd.pos2.getBlockX()); x++) {
            p.getWorld().getBlockAt(new Location(p.getWorld(), x + 0.5d, pd.pos1.getY() + 0.5d, pd.pos1.getZ() + 0.5d)).setType(Material.getMaterial(args[0]));
            p.getWorld().getBlockAt(new Location(p.getWorld(), x + 0.5d, pd.pos2.getY() + 0.5d, pd.pos1.getZ() + 0.5d)).setType(Material.getMaterial(args[0]));
            p.getWorld().getBlockAt(new Location(p.getWorld(), x + 0.5d, pd.pos1.getY() + 0.5d, pd.pos2.getZ() + 0.5d)).setType(Material.getMaterial(args[0]));
            p.getWorld().getBlockAt(new Location(p.getWorld(), x + 0.5d, pd.pos2.getY() + 0.5d, pd.pos2.getZ() + 0.5d)).setType(Material.getMaterial(args[0]));
        }
        for (int z = Math.min(pd.pos1.getBlockZ(), pd.pos2.getBlockZ()); z <= Math.max(pd.pos1.getBlockZ(), pd.pos2.getBlockZ()); z++) {
            p.getWorld().getBlockAt(new Location(p.getWorld(), pd.pos1.getX() + 0.5d, pd.pos1.getY() + 0.5d, z + 0.5d)).setType(Material.getMaterial(args[0]));
            p.getWorld().getBlockAt(new Location(p.getWorld(), pd.pos2.getX() + 0.5d, pd.pos1.getY() + 0.5d, z + 0.5d)).setType(Material.getMaterial(args[0]));
            p.getWorld().getBlockAt(new Location(p.getWorld(), pd.pos1.getX() + 0.5d, pd.pos2.getY() + 0.5d, z + 0.5d)).setType(Material.getMaterial(args[0]));
            p.getWorld().getBlockAt(new Location(p.getWorld(), pd.pos2.getX() + 0.5d, pd.pos2.getY() + 0.5d, z + 0.5d)).setType(Material.getMaterial(args[0]));
        }
        for (int y = Math.min(pd.pos1.getBlockY(), pd.pos2.getBlockY()); y <= Math.max(pd.pos1.getBlockY(), pd.pos2.getBlockY()); y++) {
            p.getWorld().getBlockAt(new Location(p.getWorld(), pd.pos1.getX() + 0.5d, y + 0.5d, pd.pos1.getZ() + 0.5d)).setType(Material.getMaterial(args[0]));
            p.getWorld().getBlockAt(new Location(p.getWorld(), pd.pos2.getX() + 0.5d, y + 0.5d, pd.pos1.getZ() + 0.5d)).setType(Material.getMaterial(args[0]));
            p.getWorld().getBlockAt(new Location(p.getWorld(), pd.pos1.getX() + 0.5d, y + 0.5d, pd.pos2.getZ() + 0.5d)).setType(Material.getMaterial(args[0]));
            p.getWorld().getBlockAt(new Location(p.getWorld(), pd.pos2.getX() + 0.5d, y + 0.5d, pd.pos2.getZ() + 0.5d)).setType(Material.getMaterial(args[0]));
        }

        p.sendMessage(ChatColor.YELLOW + "Updated " + i + " blocks in " + new DecimalFormat("#.######").format(((System.nanoTime() - startTime) / 1000000d)) + " ms.");

        return true;
    }

}
