package net.thearcanebrony.essentialtools.wrappers;

import net.thearcanebrony.essentialtools.Main;
import org.bukkit.Server;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.Listener;

public class ETEventHandler implements Listener {
    public static FileConfiguration getConfig() {
        return Main.getPlugin().getConfig();
    }
    public static Server getServer() { return Main.getPlugin().getServer(); }
}
