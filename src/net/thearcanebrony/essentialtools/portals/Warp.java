package net.thearcanebrony.essentialtools.portals;

import net.thearcanebrony.essentialtools.Main;
import net.thearcanebrony.essentialtools.Util;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

public class Warp {
    public Warp(Player p, String warpName) {
        Util.logDebug("1 " + p.getName() + " " + warpName);
        String worldName = Main.getPlugin().getConfig().getString("Warps." + warpName + ".world");
        Util.logDebug("2 Name: " + worldName);
        World world = Bukkit.getWorld(worldName);
        Util.logDebug("3");
        double x = (double) Main.getPlugin().getConfig().getDouble("Warps." + warpName + ".x");
        double y = (double) Main.getPlugin().getConfig().getDouble("Warps." + warpName + ".y");
        double z = (double) Main.getPlugin().getConfig().getDouble("Warps." + warpName + ".z");
        float yaw = (float) Main.getPlugin().getConfig().getLong("Warps." + warpName + ".yaw");
        float pitch = (float) Main.getPlugin().getConfig().getLong("Warps." + warpName + ".pitch");

        Util.logDebug("4 " + x + "/" + y + "/ " + z + " | " + yaw + "/" + pitch);
        p.getPlayer().teleport(new Location(world, x, y, z, yaw, pitch));
    }
}