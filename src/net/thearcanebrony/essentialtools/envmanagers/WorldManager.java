package net.thearcanebrony.essentialtools.envmanagers;

import net.thearcanebrony.essentialtools.CommandHandler;
import net.thearcanebrony.essentialtools.Main;
import org.bukkit.*;
import org.bukkit.World.Environment;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;


public class WorldManager extends CommandHandler implements Listener {
    public WorldManager(Main plugin) {
        super(plugin);
    }

	/*public Whois(JavaPluginLoader loader, PluginDescriptionFile description, File dataFolder, File file) {
		super(loader, description, dataFolder, file);
		// TODO Auto-generated constructor stub
	}*/


    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player p = Bukkit.getPlayer(sender.getName());
        if (command.getName().equalsIgnoreCase("createworld")) {
            if (!sender.hasPermission("thearcanetest.createworld")) {
                sender.sendMessage(ChatColor.RED + "You do not have permission create worlds");
                return true;
            }
            if (args.length == 0) {

                sender.sendMessage(ChatColor.GRAY + "Please specify a world name");

                return true;
            } else if (args.length == 1) {

                WorldCreator worldcreator = new WorldCreator(args[0]);
                worldcreator.environment(Environment.NORMAL);
                Bukkit.createWorld(worldcreator);
                World world = Bukkit.getWorld(args[0]);
                Location location = new Location(world, world.getSpawnLocation().getX(), world.getSpawnLocation().getY(), world.getSpawnLocation().getZ());
                p.teleport(location);
                return true;
            }


        }
        return false;
    }


}
