package net.thearcanebrony.essentialtools.datastorage;

import org.bukkit.Location;
import org.bukkit.Server;

public class WEPerPlayerData {
    public Server server;
    public Location pos1 = new Location(null, 0, 0, 0), pos2 = new Location(null, 0, 0, 0);
    public boolean seeBarriers = false;
    public WEPerPlayerData(Server s) {
        server = s;
    }
}