package net.thearcanebrony.essentialtools.datastorage;

import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import java.util.HashMap;

public class PerPlayerData {
    public static HashMap<Player, PerPlayerData> playerData = new HashMap<Player, PerPlayerData>();
    public Server server;
    public long timeSinceLastBreak = 0;
    public Scoreboard scoreboard;
    public Objective objective;

    public PerPlayerData(Server s) {
        server = s;
    }

    public static PerPlayerData getPlayerData(Player p) {

        return playerData.getOrDefault(p, getDefaultPlayerData(p));
    }

    public static PerPlayerData getDefaultPlayerData(Player p) {
        PerPlayerData dpd = new PerPlayerData(p.getServer());
        playerData.putIfAbsent(p, dpd);
        return playerData.getOrDefault(p, dpd);
    }
}