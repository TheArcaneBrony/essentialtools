package net.thearcanebrony.essentialtools.datastorage;

import org.bukkit.Instrument;
import org.bukkit.Note;

public class NoteData {
    public Instrument instrument = Instrument.PIANO;
    public Note note = new Note(10);

    public NoteData() {
    }
    public NoteData(Instrument _instrument, Note _note) {
        instrument = _instrument;
        note = _note;
    }
}
