package net.thearcanebrony.essentialtools.datastorage;

import java.util.ArrayList;
import java.util.HashMap;

public class SongData {
    public String Name = "Untitled";
    public long TickDelay = 1;
    public HashMap<Long, ArrayList<NoteData>> Notes = new HashMap<>();
}
