package net.thearcanebrony.essentialtools.datastorage;

import net.thearcanebrony.essentialtools.Main;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

import java.util.HashMap;

public class WETaskManager {
    public static HashMap<String, Task> tasks = new HashMap<String, Task>();

    public WETaskManager() {
        // TODO Auto-generated constructor stub
    }

    public static void AddTask(Player p, Task task) {
        tasks.put(tasks.size() + "_" + p.getName(), task);
    }

    public static int CancelTasks(Player p) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.getPlugin(), new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 1; i++) {
                    for (String taskname : tasks.keySet()) {
                        if (taskname.endsWith(p.getName())) {
                            Task task = tasks.get(taskname);
                            task.Canceled = true;
                            for (int j = task.tasks.values().size() - 1; j >= 0; j--) {
                                BukkitTask t = (BukkitTask) task.tasks.values().toArray()[j];
                                t.cancel();

                            }
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                            //task.tasks.clear();


                        }
                        //tasks.remove(taskname);


                    }
                }
            }
        });
        return 0;
    }


    public static class Task {
        public boolean Canceled = false;
        public HashMap<String, BukkitTask> tasks = new HashMap<String, BukkitTask>();

    }
}
