package net.thearcanebrony.essentialtools.commands;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.thearcanebrony.essentialtools.CommandHandler;
import net.thearcanebrony.essentialtools.Main;
import net.thearcanebrony.essentialtools.RuntimeStore;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RedstoneEventCount extends CommandHandler implements Listener {
    byte cpusamples = 100;
    DecimalFormat df = new DecimalFormat("00.00");

    public RedstoneEventCount(Main plugin) {
        super(plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player p = Bukkit.getPlayer(sender.getName());
        if (args.length == 0) {
            if (!sender.hasPermission("essentialtools.stats")) {
                sender.sendMessage(ChatColor.RED + "You do not have permission to get server stats");
                return true;
            } else {
                p.sendMessage("Building list of redstone activity.");
                HashMap<String, Integer> positions = new HashMap<>();
                for (Map.Entry<Long, ArrayList<Integer[]>> i : RuntimeStore.RedstoneEvents.entrySet()) {
                    for (Integer[] j : i.getValue()) {
                        String id = j[0] + "/" + j[1];
                        if (!positions.containsKey(id)) positions.put(id, 0);
                        positions.replace(id, positions.get(id) + 1);
                    }
                }
                p.sendMessage("Done building list of redstone activity.\nClick any of the following entries to teleport.\n");
                for (Map.Entry<String, Integer> i : positions.entrySet()) {
                    String[] idp = i.getKey().split("/");
                    int x = Integer.parseInt(idp[0]), z = Integer.parseInt(idp[1]);

                    TextComponent msg = new TextComponent();
                    msg.setText(i.getKey() + ": " + i.getValue() + " events.");
                    msg.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/minecraft:tp " + (x * 16) + " " + p.getWorld().getHighestBlockYAt(x, z) + " " + (z * 16)));
                    p.spigot().sendMessage(msg);
                }
            }
            return true;
        }
        return false;
    }

    private double calculateAverage(List<Double> list) {
        Double sum = 0d;
        if (!list.isEmpty()) {
            for (Double num : list) {
                sum += num;
            }
            return sum / list.size();
        }
        return sum;
    }

}
