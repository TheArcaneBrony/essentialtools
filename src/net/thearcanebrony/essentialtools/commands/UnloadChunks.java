package net.thearcanebrony.essentialtools.commands;

import net.thearcanebrony.essentialtools.CommandHandler;
import net.thearcanebrony.essentialtools.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;


public class UnloadChunks extends CommandHandler implements Listener {
    public UnloadChunks(Main plugin) {
        super(plugin);
    }

	/*public Whois(JavaPluginLoader loader, PluginDescriptionFile description, File dataFolder, File file) {
		super(loader, description, dataFolder, file);
		// TODO Auto-generated constructor stub
	}*/


    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player p = Bukkit.getPlayer(sender.getName());
        if (args.length == 0) {
            if (!sender.hasPermission("thearcanetest.unloadchunks")) {
                sender.sendMessage(ChatColor.RED + "You do not have permissions to unload chunks!");
                return true;
            }
            sender.getServer().broadcastMessage(p.getDisplayName() + " is unloading chunks");
            //sender.sendMessage(ChatColor.YELLOW + Bukkit.getPlayer(sender.getName()).getDisplayName());
            int i = 0;
            int i2 = p.getWorld().getLoadedChunks().length;
            for (Chunk c : p.getWorld().getLoadedChunks()) {
                i++;
                p.getWorld().unloadChunk(c);
                Integer.toString(p.getWorld().getLoadedChunks().length);
                p.sendMessage("Unloading chunk " + i + "/" + i2 + " (" + (i2 - i) + " left)");
            }

            return true;
        }

        return false;
    }


}
