package net.thearcanebrony.essentialtools.commands;

import net.thearcanebrony.essentialtools.CommandHandler;
import net.thearcanebrony.essentialtools.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;


public class CreateWarp extends CommandHandler implements Listener {
    public CreateWarp(Main plugin) {
        super(plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        String world = "null";
        double x = 0;
        double y = 0;
        double z = 0;
        float yaw = 0f;
        float pitch = 0f;
        String warpname = "null";
        Player p = Bukkit.getPlayer(sender.getName());
        if (args.length == 0) {
            p.sendMessage(ChatColor.RED + "Please provide a warp name!");
            return true;
        } else {
            warpname = args[0];
            world = p.getLocation().getWorld().getName();
            if (args.length == 1) {
                x = p.getLocation().getX();
                y = p.getLocation().getY();
                z = p.getLocation().getZ();
                yaw = p.getLocation().getYaw();
                pitch = p.getLocation().getPitch();
            } else if (args.length == 4) {
                x = Double.parseDouble(args[1]);
                y = Double.parseDouble(args[2]);
                z = Double.parseDouble(args[3]);
                yaw = 0f;
                pitch = 0f;
            } else if (args.length == 6) {
                x = Double.parseDouble(args[1]);
                y = Double.parseDouble(args[2]);
                z = Double.parseDouble(args[3]);
                yaw = Float.parseFloat(args[4]);
                pitch = Float.parseFloat(args[5]);
            } else {
                p.sendMessage(ChatColor.RED + "Invalid number of arguments entered!\nValid syntax: /createwarp name [x y z [yaw pitch]]");
                return true;
            }
            String warp = "Warps." + warpname + ".";
            Main.getPlugin().getConfig().createSection("Warps." + warpname);
            Main.getPlugin().getConfig().set(warp + "world", world);
            Main.getPlugin().getConfig().set(warp + "x", x);
            Main.getPlugin().getConfig().set(warp + "y", y);
            Main.getPlugin().getConfig().set(warp + "z", z);
            Main.getPlugin().getConfig().set(warp + "yaw", yaw);
            Main.getPlugin().getConfig().set(warp + "pitch", pitch);
            Main.getPlugin().saveConfig();
            sender.sendMessage(ChatColor.BOLD + "-------------------ADDWARP-------------------"
                    + ChatColor.RESET
                    + "Added warp: " + warpname + "\n"
                    + "  -     x: " + x + "\n"
                    + "  -     y: " + y + "\n"
                    + "  -     z: " + z + "\n"
                    + "  -   yaw: " + yaw + "\n"
                    + "  - pitch: " + pitch + "\n"
                    + "  - world: " + world + "\n"
                    + ChatColor.BOLD + "---------------------------------------------");
        }


        return true;
    }
}