package net.thearcanebrony.essentialtools.commands;

import com.google.gson.Gson;
import net.thearcanebrony.essentialtools.CommandHandler;
import net.thearcanebrony.essentialtools.Main;
import net.thearcanebrony.essentialtools.datastorage.NoteData;
import net.thearcanebrony.essentialtools.datastorage.SongData;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static net.thearcanebrony.essentialtools.Main.getPlugin;

public class MusicPlayer extends CommandHandler implements Listener {
    DecimalFormat df = new DecimalFormat("00.00");

    Gson gson = new Gson();

    public MusicPlayer(Main plugin) {
        super(plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player p = Bukkit.getPlayer(sender.getName());
        if (args.length == 0) {
            if (!sender.hasPermission("essentialtools.music.play")) {
                sender.sendMessage(ChatColor.RED + "You do not have permission to play music!");
                return true;
            } else {
                SongData sd;
                try {
                    FileReader fr = new FileReader("song.json");
                    sd = gson.fromJson(fr, SongData.class);
                    fr.close();
                    p.sendMessage("Playing: " + sd.Name);
                    p.sendMessage("Tick delay: " + sd.TickDelay + " ticks");
                    p.sendMessage("Duration: " + sd.Notes.size() + " ticks (" + sd.Notes.size() / (20D / sd.TickDelay) + "s)");
                    int notes = 0;
                    for (Map.Entry<Long, ArrayList<NoteData>> set : sd.Notes.entrySet()) {
                        notes += set.getValue().size();
                    }
                    p.sendMessage("Notes: " + notes);
                    BossBar bb = Bukkit.createBossBar("Music track...", BarColor.GREEN, BarStyle.SEGMENTED_20);
                    bb.addPlayer(p);
                    BukkitTask bt = new BukkitRunnable() {
                        int currentTime = 0;

                        @Override
                        public void run() {
                            if (currentTime < sd.Notes.size()) {
                                Map.Entry<Long, ArrayList<NoteData>> item = (Map.Entry<Long, ArrayList<NoteData>>) sd.Notes.entrySet().toArray()[currentTime];
                                for (Map.Entry<Long, ArrayList<NoteData>> set : sd.Notes.entrySet()) {
                                    if (set.getKey().equals((long) currentTime)) {
                                        String bbt = sd.Name + " " + currentTime + "/" + sd.Notes.size() + " - ";
//                                        for(NoteData nd : set.getValue()){
//                                            bbt += nd.instrument + ":" + nd.note + ", ";
//                                        }
                                        bbt += set.getValue().size() + " instruments";
                                        bb.setTitle(bbt);
                                        bb.setProgress(currentTime / (double) sd.Notes.size());
                                        for (NoteData nd : set.getValue()) {
                                            //p.sendMessage(nd.instrument + " " + nd.note);
                                            p.playNote(p.getLocation(), nd.instrument, nd.note);
                                        }
                                    }
                                }
                                currentTime++;
                            } else {
                                bb.setTitle(sd.Name + ": Finished playing!");
                                try {
                                    Thread.sleep(2000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                bb.removeAll();
                            }
                        }
                    }.runTaskTimerAsynchronously(getPlugin(), 0L, sd.TickDelay);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return true;
        }
        return false;
    }

    private double calculateAverage(List<Double> list) {
        Double sum = 0d;
        if (!list.isEmpty()) {
            for (Double num : list) {
                sum += num;
            }
            return sum / list.size();
        }
        return sum;
    }

}
