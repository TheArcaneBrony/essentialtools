package net.thearcanebrony.essentialtools.commands;

import net.thearcanebrony.essentialtools.CommandHandler;
import net.thearcanebrony.essentialtools.Main;
import net.thearcanebrony.essentialtools.events.WhoisRequestEvent;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;

import java.util.Collection;
import java.util.Date;
import java.util.Set;

public class Whois extends CommandHandler {
    public Whois(Main plugin) {
        super(plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 0) {
            if (!sender.hasPermission("chatbukkit.who.list")) {
                sender.sendMessage(ChatColor.RED + "You do not have permission to view the online players");
                return true;
            }

            PerformPlayerList(sender, args);
            return true;
        } else if (args.length == 1) {
            if (!sender.hasPermission("chatbukkit.who.whois")) {
                sender.sendMessage(ChatColor.RED + "You do not have permission to view their details");
                return true;
            }

            PerformWhois(sender, args);
            return true;
        }

        return false;
    }

    @SuppressWarnings("deprecation")
    private void PerformWhois(CommandSender sender, String[] args) {
        Player player = getPlayer(sender, args, 0);

        if (player != null) {
            WhoisRequestEvent report = new WhoisRequestEvent(sender, player);
            report.setField("Display Name", player.getDisplayName());
            report.setField("World", player.getWorld().getName());

            if (!ChatColor.stripColor(player.getDisplayName()).equalsIgnoreCase(player.getName())) {
                report.setField("Username", player.getName());
            }
            report.setField("IP", player.getAddress().toString());
            report.setField("Flying", String.valueOf(player.isFlying()) + " " + (player.getAllowFlight() ? "[a]" : ""));
            if (player.getActivePotionEffects().size() >= 1) {
                String userpots = "";
                int i = 0;
                for (PotionEffect pt : player.getActivePotionEffects()) {
                    i++;
                    if (i == 1) {
                        userpots = pt.getType().getName();
                    } else {
                        userpots = userpots + ", " + pt.getType().getName();
                    }
                }

                report.setField("Potions", userpots);
            }
            report.setField("Health", player.getHealth() + "/" + player.getMaxHealth() + " (Scaled: "
                    + player.isHealthScaled() + ", Scale: " + player.getHealthScale() + ")");
            report.setField("Experience", player.getExp() + " (" + player.getExpToLevel() + " left) (Total: "
                    + player.getTotalExperience() + ")");
            report.setField("UUID", player.getUniqueId().toString());
            report.setField("OP", player.isOp() + "");
            report.setField("Online", player.isOnline() + "");
            report.setField("Food", player.getFoodLevel() + "/20");
            report.setField("Whitelisted", player.isWhitelisted() + "");
            report.setField("Language", player.getLocale());
            report.setField("Velocity", player.getVelocity().normalize().length() + "");
            report.setField("Locale", player.getLocale());
            report.setField("View distance", player.getClientViewDistance() + "");
            report.setField("First played (long)", new Date(player.getFirstPlayed()) + "");

            player.getServer().getPluginManager().callEvent(report);
            sender.sendMessage("------ WHOIS report ------");
            Set<String> keys = report.getFields().keySet();

            for (String key : keys) {
                sender.sendMessage(key + ": " + report.getField(key));
            }
        }
    }

    private void PerformPlayerList(CommandSender sender, String[] args) {
        String result = "";
        Collection<? extends Player> players = plugin.getServer().getOnlinePlayers();
        int count = 0;

        for (Player player : players) {
            String name = player.getDisplayName();

            if (name.length() > 0) {
                if (result.length() > 0) {
                    result += ", ";
                }
                result += name;
                count++;
            }
        }

        if (count == 0) {
            sender.sendMessage("There's currently nobody playing on this server!");
        } else if (count == 1) {
            sender.sendMessage("There's only one player online: " + result);
        } else {
            sender.sendMessage("Online players: " + result);
        }
    }

}
