package net.thearcanebrony.essentialtools.commands;

import net.thearcanebrony.essentialtools.CommandHandler;
import net.thearcanebrony.essentialtools.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

public class Clagg extends CommandHandler implements Listener {
    public Clagg(Main plugin) {
        super(plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player p = Bukkit.getPlayer(sender.getName());
        World w = p.getWorld();
        if (args.length == 0) {
            if (!sender.hasPermission("thearcanetest.clagg")) {
                sender.sendMessage(ChatColor.RED + "You do not have permissions to clear lagg!");
                return true;
            }
            sender.getServer().broadcastMessage(ChatColor.GREEN + p.getDisplayName() + ChatColor.GREEN
                    + " is clearing the " + ChatColor.DARK_RED + ChatColor.BOLD + ChatColor.UNDERLINE + "LAGG");
            int i2 = 0;

            Player p2 = p.getPlayer();
            for (Entity E2 : w.getLivingEntities()) {
                String en2 = E2.getType().name();
                if (en2 != "PLAYER" && en2 != "ITEM_FRAME" && en2 != "VILLAGER" && en2 != "PAINTING"
                        && en2 != "ARMOR_STAND" && en2 != "DROPPED_ITEM" && !en2.contains("CART") && en2 != "EXPERIENCE_ORB") {
                    i2++;
                    p2.sendTitle("Clearing lagg", "Detecting Entities: " + i2, 20, 20, 20);
                    // p.sendMessage(newlines+"FOUND "+en2 + "i2 = " +
                    // i2);
                }


            }
            for (Entity e : w.getLivingEntities()) {
                String en = e.getType().name();
                if (en != "PLAYER" && en != "ITEM_FRAME" && en != "VILLAGER" && en != "PAINTING"
                        && en != "ARMOR_STAND" && en != "DROPPED_ITEM" && !en.contains("MINECART") && en != "EXPERIENCE_ORB") {
                    e.remove();
					
					/*e.setInvulnerable(false);
					
					e.setGlowing(false);
					e.setCustomName("Ascending!");
					e.setCustomNameVisible(true);
					e.teleport(new Location(e.getWorld(), e.getLocation().getX(), 5, e.getLocation().getZ()));
					//e.teleport(new Location(e.getWorld(), 0, e.getLocation().getY() + 0.1, 0));
					e.setFallDistance(60000);
					e.setFireTicks(5000000);
					e.setGravity(true);
					e.setVelocity(new Vector(0, 0, 0));
					e.eject();*/


                }
            }

            //	i2 = w.getLoadedChunks().length;
            w.setKeepSpawnInMemory(false);
            //w.setAmbientSpawnLimit(0);
            //	w.setAnimalSpawnLimit(0);
            //	w.setDifficulty(Difficulty.PEACEFUL);
            //	w.setMonsterSpawnLimit(0);
            //		w.setStorm(false);
            //		w.setThundering(false);

            for (Chunk c : w.getLoadedChunks()) {
                w.unloadChunk(c);
                // p.sendMessage(String.format(newlines + "Unloading chunk " + i
                // + "/" + i2 + " (" + (i2 - i) + " left)"));
            }


            w.setKeepSpawnInMemory(true);
            return true;
        }

        return false;
    }

}
