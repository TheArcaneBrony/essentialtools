package net.thearcanebrony.essentialtools.commands;

import net.thearcanebrony.essentialtools.CommandHandler;
import net.thearcanebrony.essentialtools.Main;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

public class ChestGUI extends CommandHandler {
    /*public Whois(JavaPluginLoader loader, PluginDescriptionFile description, File dataFolder, File file) {
		super(loader, description, dataFolder, file);
		// TODO Auto-generated constructor stub
	}*/
    BukkitTask gui = null;
    String title = "air";
    int scroll = 0;
    String[] test = {"§cWelcome to AstroPlexMC", "§4Welcome to AstroPlexMC", "§6Welcome to AstroPlexMC", "§eWelcome to AstroPlexMC", "§aWelcome to AstroPlexMC", "§9Welcome to AstroPlexMC", "§1Welcome to AstroPlexMC", "§5Welcome to AstroPlexMC", "§dWelcome to AstroPlexMC"};
    public ChestGUI(Main plugin) {
        super(plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        gui(sender);

        return true;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    void onInventoryClick(InventoryClickEvent event) {

        @SuppressWarnings("unused")
        int slot = event.getRawSlot();
        final Player p = (Player) event.getWhoClicked();
        p.closeInventory();
        gui.getTaskId();
        Bukkit.getScheduler().cancelTasks(Main.getPlugin());
        //Bukkit.getScheduler().cancelAllTasks();

    }

    final void gui(CommandSender sender) {
        @SuppressWarnings("unused") final BukkitTask gui = new BukkitRunnable() {
            @Override
            public void run() {
                /*for(Player player : Bukkit.getOnlinePlayers())
                {
                    player.getInventory().addItem(new ItemStack(Material.MOSSY_COBBLESTONE, 1));
                }*/
                title = test[(scroll % test.length)];
                Inventory gui = Bukkit.createInventory((InventoryHolder) sender, 9, title);

                gui.addItem(new ItemStack(Material.STONE));
                Bukkit.getPlayer(sender.getName()).openInventory(gui);
                scroll++;
            }

            final void stop() {
            }
        }.runTaskTimer(plugin, 0L, 5L);
    }
}
