package net.thearcanebrony.essentialtools.commands;

import net.thearcanebrony.essentialtools.CommandHandler;
import net.thearcanebrony.essentialtools.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;


public class Nick extends CommandHandler implements Listener {
    public Nick(Main plugin) {
        super(plugin);
    }

	/*public Whois(JavaPluginLoader loader, PluginDescriptionFile description, File dataFolder, File file) {
		super(loader, description, dataFolder, file);
		// TODO Auto-generated constructor stub
	}*/


    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 0) {
            if (!sender.hasPermission("thearcanetest.nick.view")) {
                sender.sendMessage(ChatColor.RED + "You do not have permission to view your current nickname");
                return true;
            }
            sender.sendMessage(ChatColor.YELLOW + Bukkit.getPlayer(sender.getName()).getDisplayName());


            return true;
        } else if (args.length == 1) {
            if (!sender.hasPermission("thearcanetest.nick.set")) {
                sender.sendMessage(ChatColor.RED + "You do not have permission to set your nickname");
                return true;
            }
            Player p = (Player) sender;
            String newnick = args[0].replaceAll("&", "§");
            p.setDisplayName(newnick + "§r");
            plugin.getConfig().set("Players." + p.getName() + ".nick", p.getDisplayName());
            plugin.saveConfig();
            sender.sendMessage(ChatColor.YELLOW + "Your nickname has been set to " + newnick);
            return true;
        }

        return false;
    }


}
