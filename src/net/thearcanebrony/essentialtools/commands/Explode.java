package net.thearcanebrony.essentialtools.commands;

import net.thearcanebrony.essentialtools.CommandHandler;
import net.thearcanebrony.essentialtools.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;


public class Explode extends CommandHandler implements Listener {
    public Explode(Main plugin) {
        super(plugin);
    }


    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player p = Bukkit.getPlayer(sender.getName());
        if (args.length == 0) {
            if (!sender.hasPermission("thearcanetest.explode")) {
                sender.sendMessage(ChatColor.RED + "You do not have permission to explode");
                return true;
            }
            p.getWorld().createExplosion(p.getLocation(), 3);

            return true;
        } else if (args.length == 1) {
            int size = Integer.parseInt(args[0]);
            p.getWorld().createExplosion(p.getLocation(), size);

            return true;
        }

        return false;
    }


}
