package net.thearcanebrony.essentialtools.commands;

import net.thearcanebrony.essentialtools.CommandHandler;
import net.thearcanebrony.essentialtools.Main;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;

public class Discord extends CommandHandler implements Listener {
    public Discord(Main plugin) {
        super(plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        sender.sendMessage(ChatColor.RESET + "Discord: https://discord.gg/zSg4SVS");

        return true;
    }

}
