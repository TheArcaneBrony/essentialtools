package net.thearcanebrony.essentialtools.commands;

import com.sun.management.OperatingSystemMXBean;
import net.thearcanebrony.essentialtools.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.*;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;

import java.lang.management.ManagementFactory;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.*;

public class Stats extends CommandHandler implements Listener {
    byte cpusamples = 100;
    DecimalFormat df = new DecimalFormat("00.00");
    BossBar mem = Bukkit.createBossBar("Memory...", BarColor.BLUE, BarStyle.SEGMENTED_20);
    BossBar tps = Bukkit.createBossBar("TPS...", BarColor.PINK, BarStyle.SEGMENTED_20);
    BossBar ent = Bukkit.createBossBar("Entities...", BarColor.PINK, BarStyle.SEGMENTED_20);
    BossBar cpu = Bukkit.createBossBar("CPU...", BarColor.GREEN, BarStyle.SEGMENTED_20);
    BossBar rs = Bukkit.createBossBar("Redstone...", BarColor.GREEN, BarStyle.SEGMENTED_20);
    public List<BossBar> bossbars = new ArrayList<>(Arrays.asList(mem, cpu, tps, ent, rs));
    OperatingSystemMXBean osBean = ManagementFactory.getPlatformMXBean(
            OperatingSystemMXBean.class);
    Double cmem, ctps;
    int entities;
    Long MemUsed, MemMax, MemAlloc;
    short items = 0, players = 0, minecarts = 0, peaceful = 0, hostile = 0, others = 0;
    short maxping = 0;
    int maxrs = 0;
    ArrayList<Double> proccpu = new ArrayList<>(), syscpu = new ArrayList<>();
    Boolean pingDanger = false;
    Map<String, String> entTypes = new HashMap<>();
    long rsPeakTime = 0;

    public Stats(Main plugin) {
        super(plugin);
    }

    public void onEnable() {

        df.setRoundingMode(RoundingMode.CEILING);
        MemMax = Runtime.getRuntime().maxMemory() / 1024;

        entTypes.put("Unknown", "");
        entTypes.put("Hostile", "");
        entTypes.put("Minecart", "");
        entTypes.put("Peaceful", "");
        entTypes.put("Items", "");
        entTypes.put("Other", "");
        new BukkitRunnable() {
            @Override
            public void run() {
                if (Bukkit.getOnlinePlayers().size() == 0) return;
                MemUsed = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
                MemAlloc = Runtime.getRuntime().totalMemory() / 1048576;
                cmem = (MemUsed.doubleValue() / MemMax.doubleValue()) / 1024;
                ctps = Lag.getTPS();
                proccpu.add(osBean.getProcessCpuLoad() * 100);
                syscpu.add(osBean.getSystemCpuLoad() * 100);
                if (proccpu.size() > cpusamples) {
                    proccpu.remove(0);
                }
                if (syscpu.size() > cpusamples) {
                    syscpu.remove(0);
                }
                mem.setProgress(cmem);
                mem.setTitle(ChatColor.BLUE + "Mem: " + (MemUsed / 1048576) + " MB/" + MemMax / 1024 + " MB (" + MemAlloc + " MB alloc)");
                tps.setTitle(ChatColor.LIGHT_PURPLE + "TPS: " + df.format(ctps) + " (" + df.format(Util.getMspt()) + " MSPT)");
                cpu.setTitle(ChatColor.GREEN + "ProcCPU: " + df.format(calculateAverage(proccpu)) + "% | SysCPU: " + df.format(calculateAverage(syscpu)) + "%");
                tps.setColor(BarColor.PINK);
                if (ctps > 21) tps.setColor(BarColor.YELLOW);
                if (ctps > 20) {
                    ctps = 20D;
                    tps.setColor(BarColor.PURPLE);
                } else if (ctps < 18) {
                    tps.setColor(BarColor.YELLOW);
                    if (ctps < 15)
                        tps.setColor(BarColor.RED);
                }
                tps.setProgress(ctps / 20);

                double _syscpu = calculateAverage(syscpu);
                if (_syscpu >= 90) cpu.setColor(BarColor.RED);
                else if (ctps >= 75) cpu.setColor(BarColor.YELLOW);
                else cpu.setColor(BarColor.GREEN);
                double cpuAverage = _syscpu / 100;
                if(cpuAverage < 0) cpuAverage = 0;
                else if(cpuAverage > 1) cpuAverage = 1;
                cpu.setProgress(cpuAverage);

                pingDanger = !pingDanger;
                maxping--;
                short actualMaxPing = 0;
                short minPing = 10000;
                short totalping = 0;
                short playercount = 0;
                if (Bukkit.getOnlinePlayers().size() == 0) return;
                for (Player p : Bukkit.getOnlinePlayers()) {
                    short pping = (short) p.getPing();
                    if (pping > actualMaxPing) actualMaxPing = pping;
                    if (pping < minPing) minPing = pping;
                    totalping += pping;
                    playercount++;
                }
                if (actualMaxPing > maxping) maxping = actualMaxPing;

                for (BossBar b : RuntimeStore.PingBars) {
                    if (b.getPlayers().size() > 0) {
                        Player p = b.getPlayers().get(0);
                        short pping = (short) p.getPing();
                        if (pping > maxping) maxping = pping;

                        b.setColor(BarColor.GREEN);
                        if (pping > 300) b.setColor(BarColor.YELLOW);
                        if (pping > 600) b.setColor(BarColor.RED);
                        if (pping > 1000) b.setColor(pingDanger ? BarColor.RED : BarColor.WHITE);
                        double pingprog = pping / (double) maxping;
                        if (Double.isNaN(pingprog)) {
                            b.setProgress(1);
                        } else {
                            b.setProgress(pingprog);
                        }

                        b.setTitle("Ping: " + pping + "ms Min: " + minPing + "ms Avg: " + (totalping / playercount) + "ms Max: " + actualMaxPing + "ms");
                    }
                }
                int rse = RuntimeStore.RedstoneEvents.values().stream().mapToInt(ArrayList::size).sum();
               /* synchronized (RuntimeStore.RedstoneEvents){
                    var _rse = RuntimeStore.RedstoneEvents.values().iterator();
                    for (Iterator<ArrayList<Integer[]>> it = _rse; it.hasNext(); ) {
                        ArrayList<Integer[]> item = it.next();
                        rse += item.size();
                    }
                }*/

                if (System.currentTimeMillis() - rsPeakTime > 1000) {
                    maxrs /= 1.005;
                }
                if (rse >= maxrs) {
                    maxrs = rse;
                    rsPeakTime = System.currentTimeMillis();
                }
                rs.setTitle("Redstone events: " + rse + "/s | Peak: " + maxrs);
                if (maxrs == 0) maxrs = 1;
                rs.setProgress(rse / (double) maxrs);
            }
        }.runTaskTimerAsynchronously(plugin, 0L, 1L);
        List<Entity> entity_list = new ArrayList<>();
        new BukkitRunnable() {
            @Override
            public void run() {
                items = players = minecarts = peaceful = hostile = others = 0;
                for (World w : Bukkit.getServer().getWorlds())
                    for (Entity e : w.getEntities()) {
                        RuntimeStore.entities++;
                        if (e instanceof Player) {
                            players++;
                        } else if (e instanceof Monster || e instanceof Slime
                                || e instanceof Boss || e instanceof Ghast || e instanceof Shulker
                                || e instanceof Phantom) {
                            hostile++;
//                            if(!entTypes.get("Hostile").contains(e.getType().name()))
//                                entTypes.replace("Hostile", entTypes.get("Hostile") + ", " + e.getType().name());
                        } else if (e instanceof Minecart || e instanceof Boat) {
                            minecarts++;
//                            if(!entTypes.get("Minecart").contains(e.getType().name()))
//                                entTypes.replace("Minecart", entTypes.get("Minecart") + ", " + e.getType().name());

                        } else if (e instanceof Animals || e instanceof Golem || e instanceof Ambient
                                || e instanceof WaterMob) {
                            peaceful++;
//                            if(!entTypes.get("Peaceful").contains(e.getType().name()))
//                                entTypes.replace("Peaceful", entTypes.get("Peaceful") + ", " + e.getType().name());

                        } else if (e instanceof Projectile || e instanceof ArmorStand || e instanceof Villager
                                || e instanceof EvokerFangs || e instanceof AreaEffectCloud
                                || e instanceof WanderingTrader || e instanceof EnderSignal
                                || e instanceof EnderCrystal) {
                            others++;
//                            if(!entTypes.get("Other").contains(e.getType().name()))
//                                entTypes.replace("Other", entTypes.get("Other") + ", " + e.getType().name());

                        } else if (e instanceof Item || e instanceof ExperienceOrb || e instanceof Hanging) {
                            items++;
//                            if(!entTypes.get("Other").contains(e.getType().name()))
//                                entTypes.replace("Other", entTypes.get("Other") + ", " + e.getType().name());

                        } else if (e instanceof FallingBlock || e instanceof Explosive) {
                            items++;
//                            if(!entTypes.get("Other").contains(e.getType().name()))
//                                entTypes.replace("Other", entTypes.get("Other") + ", " + e.getType().name());
                        } else {
                            if (RuntimeStore.LogUnknownMobs)
                                Util.logDebug("Unknown ent type: " + e.getType().name());

//                            if(!entTypes.get("Unknown").contains(e.getType().name()))
//                                entTypes.replace("Unknown", entTypes.get("Unknown") + ", " + e.getType().name());
                        }

                    }
//                for(String key : entTypes.keySet()) {
//                    if(key.equals("Unknown")) System.out.println(key+": "+entTypes.get(key));
//                }
//                Bukkit.getServer().broadcastMessage("New hostile: "+ hostile);

                entities = items + players + minecarts + peaceful + hostile + others;
                String entTitle = "Entities: " + entities + " (";
                if (players > 0) entTitle += "Player " + players + " ";
                if (items > 0) entTitle += "Item " + items + " ";
                if (minecarts > 0) entTitle += "Cart " + minecarts + " ";
                if (peaceful > 0) entTitle += "Peace " + peaceful + " ";
                if (hostile > 0) entTitle += "Hostile " + hostile + (others == 0 ? "" : " ");
                if (others > 0) entTitle += "+" + others;


                entTitle += ")";
                ent.setTitle(entTitle);
                if (entities > 5000)
                    entities = 5000;

                ent.setProgress((double) entities / 5000);

            }
        }.runTaskTimer(plugin, 0L, 1L);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player p = Bukkit.getPlayer(sender.getName());
        if (args.length == 0) {
            if (!sender.hasPermission("essentialtools.stats")) {
                sender.sendMessage(ChatColor.RED + "You do not have permission to get server stats");
                return true;
            }
            if (mem.getPlayers().contains(p)) {
                cpu.removePlayer(p);
                mem.removePlayer(p);
                tps.removePlayer(p);
                ent.removePlayer(p);
                rs.removePlayer(p);
                List<BossBar> bars = new ArrayList<>();
                for (BossBar b : RuntimeStore.PingBars) {
                    if (b.getPlayers().size() > 0 && b.getPlayers().get(0) == p) {
                        b.removeAll();
                    }
                }
                for (BossBar b : bars) {
                    RuntimeStore.PingBars.remove(b);
                }
            } else {
                cpu.addPlayer(p);
                mem.addPlayer(p);
                tps.addPlayer(p);
                ent.addPlayer(p);
                rs.addPlayer(p);
                BossBar b = Bukkit.createBossBar("Ping...", BarColor.GREEN, BarStyle.SEGMENTED_20);
                b.addPlayer(p);
                RuntimeStore.PingBars.add(b);
                Util.logDebug("Added player " + p.getName());
            }
            return true;
        }
        return false;
    }

    private double calculateAverage(List<Double> list) {
        return list.stream()
                .mapToDouble(d -> d)
                .average()
                .orElse(0.0);
    }
}
