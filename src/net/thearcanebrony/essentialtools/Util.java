package net.thearcanebrony.essentialtools;

import net.milkbowl.vault.economy.Economy;
import net.minecraft.server.MinecraftServer;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_17_R1.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.text.DecimalFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BooleanSupplier;

public class Util {
    private static final DecimalFormat df = new DecimalFormat("#.##");
    static MinecraftServer server = null;
    static Method tickMethod = null;
    static BooleanSupplier someBool = () -> true;
    private static int linecounter = 0;
    private static Economy econ;

    public Util() {
        // TODO Auto-generated constructor stub
    }

    public static boolean CheckSurroundingBlocks(Block block, Material material) {
        Location loc = block.getLocation();
        World wld = loc.getWorld();
        double x = loc.getX(), y = loc.getY(), z = loc.getZ();
        BlockAtEquals(wld, x, y, z, material);
        return BlockAtEquals(wld, x + 1, y, z, material) || BlockAtEquals(wld, x - 1, y, z, material)
                || BlockAtEquals(wld, x, y + 1, z, material) || BlockAtEquals(wld, x, y - 1, z, material)
                || BlockAtEquals(wld, x, y, z + 1, material) || BlockAtEquals(wld, x, y + 1, z, material);
    }

    public static boolean BlockAtEquals(World wld, double x, double y, double z, Material mat) {
        return BlockEquals(wld.getBlockAt((int) x, (int) y, (int) z), mat);
    }

    public static boolean BlockEquals(Block block, Material material) {
        return block.getType() == material;
    }

    public static Plugin getPlugin() {
        return Main.getPlugin();
    }

    public static Server getServer() {
        return getPlugin().getServer();
    }

    public static long pingPlayerIcmp(Player p) {
        Instant startTime = Instant.now();
        try {
            InetAddress address = p.getAddress().getAddress();
            if (address.isReachable(2500)) {
                return Duration.between(startTime, Instant.now()).toMillis();
            }
        } catch (IOException e) {
            // Host not available, nothing to do here
        }
        return -1;
    }

    public static FileConfiguration getConfig() {
        return getPlugin().getConfig();
    }

    public static String parseVars(String in, Player p) {
        return parseVars(in, p, 0);
    }

    public static String parseVars(String in, Player p, int length) {
        String playerMoney;
        int entities;
        if (econ == null) {
            playerMoney = "Economy not initialised!";
            setupEconomy();
        } else
            playerMoney = econ.format(econ.getBalance(p));
        entities = RuntimeStore.entities;
        if (in.contains("$line")) linecounter++;
        if (linecounter > 10) linecounter = 10;
        if (length > 75) length = 75;
        return in
                .replace("$players", getServer().getOnlinePlayers().size() + "")
                .replace("$maxplayers", getServer().getMaxPlayers() + "")
                .replace("$tps", df.format(Lag.getTPS()))
                .replace("$mspt", "Not implemented")
                .replace("$mem", df.format(RuntimeStore.mempercent))
                .replace("$playername", p.getName())
                .replace("$ping", p.getPing() + "")
                .replace("$icmp", pingPlayerIcmp(p) + "")
                .replace("$hp", df.format(p.getHealth()))
                .replace("$maxhp", df.format(p.getHealthScale()))
                .replace("$food", df.format(p.getFoodLevel() + (double) p.getSaturation()))
                .replace("$bal", playerMoney + "")
                .replace("$entities", entities + "")
                .replace("$line-", new String(new char[linecounter]).replace("\0", "§r") + new String(new char[length]).replace("\0", "-"))
                .replace("$line=", new String(new char[linecounter]).replace("\0", "§r") + new String(new char[length]).replace("\0", "="))
                .replace("$randomcolor", "§" + StaticData.colors[RuntimeStore.colorIteration % StaticData.colors.length].getChar())

                .replace("Â", "");
    }

    private static boolean setupEconomy() {
        if (Main.getPlugin().getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

    public static ExperienceOrb SpawnXpOrb(ExperienceOrb srcOrb, int value) {
        return SpawnXpOrb(srcOrb.getLocation(), value);
    }

    public static ExperienceOrb SpawnXpOrb(Location loc, int value) {
        ExperienceOrb newOrb = (ExperienceOrb) loc.getWorld().spawnEntity(loc, EntityType.EXPERIENCE_ORB);
        if (GetNearbyEntities(loc, 30).size() > 500) {
            GetNearestPlayer(loc, 30).giveExp(value);
            newOrb.remove();
        } else {
            newOrb.setExperience(value);
        }
        return newOrb;
    }


    public static List<Entity> GetNearbyEntities(Location loc, int maxDist){
        List<Entity> entities = new ArrayList<>();
        for (Entity e : loc.getWorld().getEntities()) {
            double distance = loc.distance(e.getLocation());
            if (distance <= maxDist) {
                entities.add(e);
            }
        }
        return entities;
    }
    public static Player GetNearestPlayer(Location loc) {
        return GetNearestPlayer(loc, Integer.MAX_VALUE);
    }

    public static Player GetNearestPlayer(Entity entity) {
        return GetNearestPlayer(entity, Integer.MAX_VALUE);
    }

    public static Player GetNearestPlayer(Entity entity, int maxDist) {
        return GetNearestPlayer(entity.getLocation(), maxDist);
    }

    public static Player GetNearestPlayer(Location loc, int maxDist) {
        Player result = null;
        double lastDistance = Double.MAX_VALUE;
        for (Player p : loc.getWorld().getPlayers()) {
            double distance = loc.distance(p.getLocation());
            if (distance < lastDistance && distance <= maxDist) {
                lastDistance = distance;
                result = p;
            }
        }

        if (result != null) {
            return result;
        } else {
            return null;
        }
    }
    private static int maxSrcLength = 0;
    public static void logDebug(String text){
        if(RuntimeStore.LogDebug) {
            StackTraceElement ste = Thread.currentThread().getStackTrace()[2];
            String[] lines = text.split("\n");
            String source = ste.getFileName() + ":" + ste.getLineNumber();
            if(source.length() >= maxSrcLength)
                maxSrcLength = source.length();
            else while (source.length() < maxSrcLength)
                source = source + " ";
            for (String line : lines) System.out.println(ste.getFileName() + ":" + ste.getLineNumber() + " -> " + line);
        }
    }

    public static void forceTickNow() throws ReflectiveOperationException {
        if (tickMethod == null) {
            server = MinecraftServer.getServer();
            tickMethod = MinecraftServer.class.getDeclaredMethod("b", BooleanSupplier.class);
            tickMethod.setAccessible(true);
        }
        tickMethod.invoke(server, someBool);
    }

    public static double getMspt(){
        long[] h = getServer().getTickTimes();
//        MinecraftServer ms = MinecraftServer.getServer();
//        int ct = 0;
//        try {
//            Field f = MinecraftServer.class.getDeclaredField("ticks");
//            f.setAccessible(true);
//            ct = (int) f.get(ms);
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        } catch (NoSuchFieldException e) {
//            e.printStackTrace();
//        }
//
//        long[] v = new long[100];
//        ct-=v.length;
//        for (int i = 0; i < v.length; i++) {
//            v[i] = h[(ct+i)%100];
//        }
        return Arrays.stream(h).average().orElse(-1)/1_000_000;
    }
}
