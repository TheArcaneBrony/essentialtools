package net.thearcanebrony.essentialtools;

import net.thearcanebrony.essentialtools.commands.*;
import net.thearcanebrony.essentialtools.datastorage.ConfigDefaults;
import net.thearcanebrony.essentialtools.events.*;
import net.thearcanebrony.essentialtools.mechanics.*;
import net.thearcanebrony.essentialtools.worldedit.*;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.boss.BossBar;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main extends JavaPlugin implements Listener {
    public static Boolean IsRunning = true;
    public static WEHandler wehandler = new WEHandler();
    private final Stats stats = new Stats(this);
    private final RedstoneEventCount redstoneEventCount = new RedstoneEventCount(this);
    private final MusicPlayer musicPlayer = new MusicPlayer(this);
    DecimalFormat df = new DecimalFormat("#.##");
    Random rnd = new Random();
    String chatPrefix = getConfig().getString("Chat Prefix");
    private static Plugin plugin;

    public static Plugin getPlugin() {
//        return Bukkit.getPluginManager().getPlugin("EssentialTools");
        return plugin;
    }


    @Override
    public void onEnable() {
        plugin = this;
        Server srv = Bukkit.getServer();
        PluginManager pm = srv.getPluginManager();
        FileConfiguration cfg = getConfig();
        ConfigDefaults.setupDefaults(cfg);
        cfg.options().copyDefaults(true);
        //cfg.setDefaults(cfg);
        try {
            cfg.save("plugins/" + getPlugin().getName() + "/config.yml");
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        srv.getScheduler().scheduleSyncRepeatingTask(this, new Lag(), 0L, 1L);
        //register commands
        getCommand("whois").setExecutor(new Whois(this));
        getCommand("unloadchunks").setExecutor(new UnloadChunks(this));
        getCommand("stats").setExecutor(stats);
        getCommand("about").setExecutor(new About(this));
        getCommand(".fill").setExecutor(new Fill(this));
        getCommand(".setbelow").setExecutor(new Setbelow(this));
        getCommand(".setbelowt").setExecutor(new SetbelowT(this));
        getCommand(".walls").setExecutor(new Walls(this));
        getCommand(".outline").setExecutor(new Outline(this));
        getCommand(".cancel").setExecutor(new Cancel(this));
        getCommand("redstoneevents").setExecutor(redstoneEventCount);
        getCommand("re").setExecutor(redstoneEventCount);
        getCommand("play").setExecutor(musicPlayer);
        //register events
        pm.registerEvents(this, this);
        pm.registerEvents(new ETBlockFromToEvent(), this);
        pm.registerEvents(new ETBlockPhysicsEvent(), this);
        pm.registerEvents(new ETBlockRedstoneEvent(), this);
        pm.registerEvents(new ETEntityDeathEvent(), this);
        pm.registerEvents(new ETPlayerExpChangeEvent(), this);
        pm.registerEvents(new ETPlayerInteractEvent(), this);
        pm.registerEvents(new ETPlayerJoinEvent(), this);
        pm.registerEvents(new ETEntityChangeBlockEvent(), this);
        pm.registerEvents(new ETTimeSkipEvent(), this);
        //run init tasks for some features
        stats.onEnable();
        BarrierView.Start();
        OldXP.Start();
        BloodMoon.Start();
        EntityHealthBar.Start();
        ScoreboardUpdater.StartTask();
        RedstoneEventTracking.Start();
        FastNight.Start();
        //reset player join data
        for (Player p : getServer().getOnlinePlayers())
            ETPlayerJoinEvent.HandlePlayerJoin(p);
        Util.logDebug("Debug output enabled! This may cause severe spam!");
    }

    //cleanup
    @Override
    public void onDisable() {
        IsRunning = false;
        List<BossBar> bars = new ArrayList<>();
        bars.addAll(stats.bossbars);
        bars.addAll(RuntimeStore.Bossbars);
        bars.addAll(RuntimeStore.PingBars);
        for (BossBar bb : bars) {
            for (Player bbp : bb.getPlayers()) {
                bb.removePlayer(bbp);
            }
        }
        for (World w : getServer().getWorlds()) {
            w.save();
            getServer().broadcastMessage(getConfig().getString("Chat Prefix") + "Saved world " + w.getName() + "!");
        }
    }

    //commands
    //TODO: Move to dedicated command classes
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player p = Bukkit.getPlayer(sender.getName()).getPlayer();
        if (command.getName().equalsIgnoreCase("whatsmyip")) {
            sender.sendMessage(chatPrefix + "Your ip is " + p.getAddress().getHostString());
            return true;
        } else if (command.getName().equalsIgnoreCase("plugincmd")) {
            p.sendMessage("/" + args[0] + " is owned by " + getServer().getPluginCommand(args[0]).getPlugin().getName());
            return true;
        } else if (command.getName().equalsIgnoreCase("callgc")) {
            p.sendMessage("Calling garbage collector");
            System.gc();
            p.sendMessage("Called garbage collector successfully");
            return true;
        } else if (command.getName().equalsIgnoreCase(".sb")) {
            WEHandler.getPlayerData(p).seeBarriers = !WEHandler.getPlayerData(p).seeBarriers;
            boolean sb = WEHandler.getPlayerData(p).seeBarriers;
            if (sb) {
                p.sendMessage(chatPrefix + "You can now see barriers!");
                RuntimeStore.barrierViewers.add(p);
            } else {
                p.sendMessage(chatPrefix + "You can no longer see barriers!");
                RuntimeStore.barrierViewers.remove(p);
            }
            return true;
        } else if (command.getName().equalsIgnoreCase("tw")) {
            getConfig().set("Water Physics", !getConfig().getBoolean("Water Physics"));
            this.saveConfig();
            if (getConfig().getBoolean("Water Physics")) {
                getServer().broadcastMessage(chatPrefix + "Water physics enabled!");
            } else {
                getServer().broadcastMessage(chatPrefix + "Water physics disabled!");
            }
            return true;
        } else if (command.getName().equalsIgnoreCase("togglesit")) {
            boolean sit = !getConfig().getBoolean("Stair Sitting");

            if (sit) {
                p.sendMessage(chatPrefix + "Sitting enabled!");
            } else {
                p.sendMessage(chatPrefix + "Sitting disabled!");
            }
            getConfig().set("Stair Sitting", sit);
            this.saveConfig();
            return true;
        } else if (command.getName().equalsIgnoreCase("togglescoreboard")) {
            boolean sit = !getConfig().getBoolean("Player Data." + p.getName() + ".Scoreboard Enabled");

            if (sit) {
                p.sendMessage(chatPrefix + "Scoreboard enabled!");
            } else {
                p.sendMessage(chatPrefix + "Scoreboard disabled!");
            }
            getConfig().set("Player Data." + p.getName() + ".Scoreboard Enabled", sit);
            this.saveConfig();
            return true;
        }
        return false;
    }
}
