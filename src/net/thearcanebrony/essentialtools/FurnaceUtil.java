package net.thearcanebrony.essentialtools;

import net.minecraft.core.BlockPosition;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.level.block.entity.TileEntity;
import net.minecraft.world.level.block.entity.TileEntityFurnaceFurnace;
import org.bukkit.Bukkit;
import org.bukkit.Keyed;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_17_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_17_R1.block.CraftFurnaceFurnace;
import org.bukkit.inventory.CookingRecipe;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;

import java.util.HashMap;
import java.util.Iterator;

public class FurnaceUtil {
    public static void DropFurnaceXP(Block bl, Location loc) {
        return;
        /*Util.SpawnXpOrb(loc, (int) FurnaceUtil.GetFurnaceXP(bl));
        CraftWorld cw = (CraftWorld) bl.getWorld();
        TileEntityFurnaceFurnace furnaceTE = (TileEntityFurnaceFurnace) cw.getHandle().getTileEntity(getFurnacePosition(bl.getLocation()));
        NBTTagCompound nbt = getNBTOfFurnace(furnaceTE);
        nbt.remove("RecipesUsed");
        CraftFurnaceFurnace craftFurnace = new CraftFurnaceFurnace(bl);
        ItemStack[] items = craftFurnace.getInventory().getContents();
        short burnTime = craftFurnace.getBurnTime();
        short cookTime = craftFurnace.getCookTime();
//        furnaceTE = (TileEntityFurnaceFurnace) furnaceTE.create(furnaceTE.getBlock(), nbt);

//        cw.getHandle().setTileEntity(getFurnacePosition(bl.getLocation()), furnaceTE);
        craftFurnace = new CraftFurnaceFurnace(bl);
        craftFurnace.getInventory().setContents(items);
        craftFurnace.setBurnTime(burnTime);
        craftFurnace.setCookTime(cookTime);

        //furnaceTE. = cookTime;
//        furnaceTE.burnTime = burnTime;*/
    }

    public static double GetFurnaceXP(Block furnace) {
        CraftWorld cw = (CraftWorld) furnace.getWorld();
        TileEntity furnaceTE = cw.getHandle().getTileEntity(getFurnacePosition(furnace.getLocation()));
        return getFurnaceXp(getRecipesUsed(getNBTOfFurnace(furnaceTE)));
    }
    //source: https://github.com/Column01/FurnaceXp/

    // Saves the furnace's NBT to a new NBTTagCompound
    static NBTTagCompound getNBTOfFurnace(TileEntity furnace) {
        NBTTagCompound nbt = new NBTTagCompound();
        return furnace.save(nbt);
    }

    static HashMap<String, Integer> getRecipesUsed(NBTTagCompound furnace) {
        NBTTagCompound recipesUsedTag = furnace.getCompound("RecipesUsed");
        HashMap<String, Integer> recipesUsed = new HashMap<>();
        for (String recipe : recipesUsedTag.getKeys()) {
            recipesUsed.put(recipe, recipesUsedTag.getInt(recipe));
        }
        return recipesUsed;
    }

    // Returns a new BlockPosition provided a Location
    static BlockPosition getFurnacePosition(Location BlockLocation) {
        return new BlockPosition(BlockLocation.getX(), BlockLocation.getY(), BlockLocation.getZ());
    }

    static double getFurnaceXp(HashMap<String, Integer> recipesUsed) {
        double furnaceXp = 0d;
        for (String recipe : recipesUsed.keySet()) {
            double exp = getRecipeExp(recipe);
            int amount = recipesUsed.get(recipe);
            furnaceXp = furnaceXp + (exp * amount);
        }
        return furnaceXp;
    }

    private static double getRecipeExp(String key) {
        // Iterates over the bukkit recipes and then returns the experience of said recipe. Called recursively for each recipe in the FurnaceRecipes array
        Iterator<Recipe> recipeIterator = Bukkit.recipeIterator();
        while (recipeIterator.hasNext()) {
            Recipe recipe = recipeIterator.next();
            if (!(recipe instanceof Keyed) || !((Keyed) recipe).getKey().toString().equals(key)) {
                continue;
            }
            if (recipe instanceof CookingRecipe) {
                return ((CookingRecipe) recipe).getExperience();
            }
            break;
        }
        return 0d;
    }
}
